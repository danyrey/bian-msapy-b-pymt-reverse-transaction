package com.bancoppel.pymt.reverse.transaction.model;

import java.time.ZonedDateTime;

import com.bancoppel.pymt.reverse.transaction.constant.ApiConstant;
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Setter;

/**
 * Error response class.
 */
@Setter
public class ErrorResponse {

	/**
	   * Type of the error.
	   */
	  private String type;
	  /**
	   * Code of the error.
	   */
	  private String code;
	  /**
	   * Detail of the error.
	   */
	  private String details;
	  /**
	   * Location of the error.
	   */
	  private String location;
	  /**
	   * Additional information of the error.
	   */
	  private String moreInfo;
	  /**
	   * UUID header from the request.
	   */
	  private String uuid;
	  /**
	   * Time when the error occurred.
	   */
	  private ZonedDateTime timestamp;

	  /**
	   * Method to get the type of the error.
	   * 
	   * @return el tipo de error.
	   */
	  public String getType() {
	    return type != null ? type : ApiConstant.EMPTY_STRING;
	  }

	  /**
	   * Method to get the code of the error.
	   * 
	   * @return codiog.
	   */
	  public String getCode() {
	    return code != null ? code : ApiConstant.EMPTY_STRING;
	  }

	  /**
	   * Method to get the detail of the error.
	   * 
	   * @return detalles.
	   */
	  public String getDetails() {
	    return details != null ? details : ApiConstant.EMPTY_STRING;
	  }

	  /**
	   * Method to get the location of the error.
	   * 
	   * @return location.
	   */
	  public String getLocation() {
	    return location != null ? location : ApiConstant.EMPTY_STRING;
	  }

	  /**
	   * Method to get additional information of the error.
	   * 
	   * @return info adicional del error.
	   */
	  public String getMoreInfo() {
	    return moreInfo != null ? moreInfo : ApiConstant.EMPTY_STRING;
	  }

	  /**
	   * Method to get the UUID.
	   * 
	   * @return uuid.
	   */
	  public String getUuid() {
	    return uuid != null ? uuid : ApiConstant.EMPTY_STRING;
	  }

	  /**
	   * Method to get the time when the error occurred with the format yyyy-MM-dd'T'HH:mm:ssZ.
	   * 
	   * @return timestamp.
	   */
	  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ssZ")
	  public ZonedDateTime getTimestamp() {
	    return timestamp;
	  }
}
