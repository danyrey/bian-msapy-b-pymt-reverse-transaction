package com.bancoppel.pymt.reverse.transaction.exception;

import java.time.ZonedDateTime;
import java.util.Map;

import com.bancoppel.pymt.reverse.transaction.constant.ApiConstant;
import com.bancoppel.pymt.reverse.transaction.constant.ConstantValidate;
import com.netflix.hystrix.exception.ExceptionNotWrappedByHystrix;

import lombok.Getter;

/**
 * Class de de tipo Exception que extiende de RuntimeException e implements
 * ExceptionNotWrappedByHystrix.
 */
@Getter
public class DownstreamException extends RuntimeException implements ExceptionNotWrappedByHystrix {

	/**
	 * Generated serial unique ID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Status of the error.
	 */
	private final int status;

	/**
	 * Type of the error.
	 */
	private final String type;

	/**
	 * Code of the error.
	 */
	private final String code;

	/**
	 * Details of the error.
	 */
	private final String details;

	/**
	 * Location of the error.
	 */
	private final String location;

	/**
	 * Additional information of the error.
	 */
	private final String moreInfo;

	/**
	 * uuid of the error.
	 */
	private final String uuid;

	/**
	 * Time stamp of the error.
	 */
	private final ZonedDateTime timestamp;

	/**
	 * Constructor que inicializa los valores de las propiedades.
	 */
	public DownstreamException() {
		super();

		this.status = ApiConstant.DEFAULT_STATUS_HTTP;
		this.type = ApiConstant.EMPTY_STRING;
		this.code = ApiConstant.EMPTY_STRING;
		this.details = ApiConstant.EMPTY_STRING;
		this.location = ApiConstant.EMPTY_STRING;
		this.moreInfo = ApiConstant.EMPTY_STRING;
		this.uuid = ApiConstant.EMPTY_STRING;
		this.timestamp = ZonedDateTime.now();

	}

	/**
	 * Constructor para llenar los valores de esta clase y pasarlos como response
	 * @param status que tendra el response.
	 * @param errorResponse Respuesta que dara error como mensaje al consultar el feing
	 */
	public DownstreamException(int status, Map<String, String> errorResponse) {
		super(ApiConstant.DOWN_STREAM_EXCEPTION_NAME + ApiConstant.SPACE_STRING + status
				+ ApiConstant.COLON + ApiConstant.SPACE_STRING
				+ errorResponse.get(ApiConstant.ERROR_RESPONSE_DETAILS_FIELD_NAME));
		this.status = status;
		this.type = errorResponse.get(ConstantValidate.ERROR_RESPONSE_TYPE);
		this.code = errorResponse.get(ConstantValidate.ERROR_RESPONSE_CODE);
		this.details = errorResponse.get(ApiConstant.ERROR_RESPONSE_DETAILS_FIELD_NAME);
		this.location = errorResponse.get(ConstantValidate.ERROR_RESPONSE_LOCATION);
		this.moreInfo = errorResponse.get(ConstantValidate.ERROR_RESPONSE_MORE_INFORMATION);
		this.uuid = errorResponse.get(ApiConstant.ERROR_RESPONSE_UUID_FIELD_NAME);
		String timeStamp = errorResponse.get(ApiConstant.ERROR_RESPONSE_TIMESTAMP_FIELD_NAME);
		timeStamp = timeStamp.substring(ConstantValidate.INT_ZERO_VALUE,
				timeStamp.length() - ConstantValidate.INT_TWO_VALUE) + ApiConstant.COLON
				+ timeStamp.substring(timeStamp.length() - ConstantValidate.INT_TWO_VALUE);
		ZonedDateTime zonedDateTime = ZonedDateTime.parse(timeStamp);
		this.timestamp = zonedDateTime;

	}

}
