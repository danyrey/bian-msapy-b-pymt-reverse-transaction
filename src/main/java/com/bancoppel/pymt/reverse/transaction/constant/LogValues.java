package com.bancoppel.pymt.reverse.transaction.constant;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import lombok.Getter;

/**
 * Clase que contiene variables para LOG.
 * 
 * @author DMR
 *
 */
@Component
@Getter
public class LogValues {
	
	  /**
	   * Tipo de log transaccional = 002.
	   */
	  @Value("${constants.log.type.transaccional}")
	  private String logTypeTransaccional;
	  
	  /**
	   * Código de transacción exitosa.
	   */
	  public static final String SUCCESS_CODE = "000000";
	  
	  /**
	   * Constante utilizada para representar el texto en la descripción del tiempo de metrica de
	   * registro en bitacora transaccional.
	   */
	  public static final String HANDLED_MESSAGE_SAVE_TRANSACTIONAL_LOG =
	      "SERVICE CALL - [Guarda en bitacora transaccional]";
	  /**
	   * Constante utilizada para representar el texto en la descripción del tiempo de metrica de
	   * registro en bitacora operacional.
	   */
	  public static final String HANDLED_MESSAGE_SAVE_OPERATIONAL_LOG =
	      "SERVICE CALL - [Guarda en bitacora operacional]";
	  
	  /**
	   * Formato de fecha para logs.
	   */
	  public static final String DATE_FORMATTER = "yyyy-MM-dd";
	  /**
	   * Formato de hora para logs.
	   */
	  public static final String TIME_FORMATTER = "HH:mm:ss";
	  
	  /**
	   * Valor de sucursal utilizado para la aplicación.
	   */
	  @Value("${constants.log.branchId}")
	  private String branchId;
	  
	  /**
	   * Ambiente en el que se está ejecutando el sistema.
	   */
	  @Value("${constants.log.coreEnvironment}")
	  private String coreEnvironment;
	  
	  /**
	   * Ambiente en el que se está ejecutando el sistema.
	   */
	  @Value("${constants.log.nameSpace}")
	  private String nameSpace;
	
	  /**
	   * Api path.
	   */
	  @Value("${constants.api.uri.mapping}")
	  private String path;
	  
	  /**
	   * Inidica tipo de bitacora.
	   */
	  public static final String STRING_ZERO_ZERO_ONE = "001";
	  
	  /**
	   * Constante que tiene la cadena para guardar en la bitcora operacional.
	   */
	  public static final String LOG_OPERATIONAL_SAVE =
	      "Tx SERVICE CALL - [Guarda en bitacora operacional], {}";
	  
	  /**
	   * Constante que tiene la cadena para guardar en la bitcora transaccional.
	   */
	  public static final String LOG_TRANSACTIONAL_SAVE =
	      "Tx SERVICE CALL - [Guarda en bitacora transaccional], {}";
	  
}
