package com.bancoppel.pymt.reverse.transaction.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.bancoppel.pymt.reverse.transaction.constant.ApiValues;
import com.bancoppel.pymt.reverse.transaction.interceptor.BexInterceptor;



/**
 * Configuration class that inherits from WebMvcConfigurer to intercept every request.
 */
@Configuration
public class InterceptorConfig implements WebMvcConfigurer {

  /**
   * Component that is executed before the request reaches the controller to validate the session.
   */
  @Autowired
  private BexInterceptor bexInterceptor;

  /**
   * Configuration API constants class.
   */
  @Autowired
  private ApiValues apiValues;

  /**
   * Overrides the addInterceptors method to define the required interceptors through the
   * corresponding registry.
   */
  @Override
  public void addInterceptors(InterceptorRegistry registry) {
    registry.addInterceptor(bexInterceptor).addPathPatterns(apiValues.getInterceptorPath());
  }
}
