/***********************************************************************
 * Copyright (c) 2023 Kairosds Solutions S.A. de C.V. All rights reserved.
 *
 * Licensed under the GNU General Public License, Version 3 (the 
 * "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at
 *
 *   https://www.gnu.org/licenses/gpl-3.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or 
 * implied.
 * 
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **********************************************************************/
package com.bancoppel.pymt.reverse.transaction.model;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;

/**
 * Moldeo que regresa la respuesta con el invoiceBranch nuero de la sucursal.
 * 
 * @author dmr
 *
 */
@ApiModel("Response")
@Getter
@Setter
public class ResponseReverse {

	private String invoiceBranch;
	private String date;
	private String time;

}
