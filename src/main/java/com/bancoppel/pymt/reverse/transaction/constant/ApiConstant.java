package com.bancoppel.pymt.reverse.transaction.constant;

import org.springframework.stereotype.Component;
import lombok.Getter;

/**
 * Clase que contiene las constantes del API.
 */
@Component
@Getter
public class ApiConstant {

	/**
	 * Constant path pattern for base path.
	 */
	public static final String BASE_PATH = "${constants.api.uri.basePath}";

	/**
	 * Endpoint del Controlador para restaurar sesion.
	 */
	public static final String API_ENDPOINT_SERVICE_PAYMENT = "${constants.api.uri.specificPaths.services}";

	/**
	 * Constante utilizada para representar el nombre de la operacion para reversos.
	 */
	public static final String OPERATION_API = "Metodo que realiza el reverso de la operacion.";

	/**
	 * Constante utilizada para representar el TSG de la operacion de reversos.
	 */
	public static final String PAYMENT_VALIDATION_TAG = "validate-service-payment";

	/**
	 * Constante utilizada para Authorization
	 */
	public static final String AUTHORIZATION = "Authorization";

	/**
	 * Constante utilizada para mostrar el mensaje en el log de la bitacora en mongo
	 */
	public static final String CUSTOMER_LOG = "CustomerNumber: {}";

	/**
	 * Constante utilizada para representar el header nombre del deviceId
	 */
	public static final String DEVICE_ID = "deviceId";

	/**
	 * Constante utilizada para representar el header nombre del uuid.
	 */
	public static final String UUID = "uuid";

	/**
	 * Constante utilizada para representar el nombre del header Date.
	 */
	public static final String DATE = "Date";

	/**
	 * Constante utilizada para representar el nombre del header Accept-Charset.
	 */
	public static final String ACCEPT_CHARSET = "Accept-Charset";

	/**
	 * Constante utilizada para representar el nombre del header Accept-Encoding.
	 */
	public static final String ACCEPT_ENCODING = "Accept-Encoding";

	/**
	 * Constante utilizada para representar el nombre del header Accept-Language.
	 */
	public static final String ACCEPT_LANGUAGE = "Accept-Language";

	/**
	 * Constante utilizada para representar el nombre del header Host.
	 */
	public static final String HOST = "Host";

	/**
	 * Constante utilizada para representar el nombre del header User-Agent.
	 */
	public static final String USER_AGENT = "User-Agent";

	/**
	 * Constante utilizada para representar el nombre del header Content-Encoding.
	 */
	public static final String CONTENT_ENCODING = "Content-Encoding";

	/**
	 * Constante utilizada para representar el nombre del header Content-Language.
	 */
	public static final String CONTENT_LANGUAGE = "Content-Language";

	/**
	 * Constante utilizada para representar el nombre del header Content-Length.
	 */
	public static final String CONTENT_LENGTH = "Content-Length";

	/**
	 * Constante utilizada para representar el nombre del header Content-MD5.
	 */
	public static final String CONTENT_MD5 = "Content-MD5";

	/**
	 * Constante que contiene el mensaje a logear de gemaltoToken.
	 */
	public static final String LOG_GEMALTO_TOKEN = "SERVICE CALL - [Valida Gemalto Token]";

	/**
	 * Constant Utilizada para mostrar el mensage en 0.
	 */
	public static final int DEFAULT_STATUS_HTTP = 0;

	/**
	 * onstante usada para representar empty string.
	 */
	public static final String EMPTY_STRING = "";

	/**
	 * Nombre de Exception.
	 */
	public static final String DOWN_STREAM_EXCEPTION_NAME = "DownstreamException";

	/**
	 * Constant para representar un espacio
	 */
	public static final String SPACE_STRING = " ";

	/**
	 * Constant para representar el string ':'.
	 */
	public static final String COLON = ":";

	/**
	 * Constant that is used to represent an index.
	 */
	public static final int INT_NEGATIVE_ONE = -1;

	/**
	 * Constant that represents number zero Integer.
	 */
	public static final int INT_ZERO_VALUE = 0;

	/**
	 * Label details para error.
	 */
	public static final String ERROR_RESPONSE_DETAILS_FIELD_NAME = "details";

	/**
	 * Label uuid of the error.
	 */
	public static final String ERROR_RESPONSE_UUID_FIELD_NAME = "uuid";

	/**
	 * Label time stamp of the error.
	 */
	public static final String ERROR_RESPONSE_TIMESTAMP_FIELD_NAME = "timestamp";

	/**
	 * Constante usada para representar el header con el nombre Accept.
	 */
	public static final String ACCEPT = "Accept";

	/**
	 * Constante utilizada para representar el nombre del header Content-Type.
	 */
	public static final String CONTENT_TYPE = "Content-Type";

	/**
	 * Constante usada para representar el header con el nombre ChannelId.
	 */
	public static final String CHANNEL_ID = "channel_id";

	/**
	 * Constant used to represent the header name of XX-Application-Name.
	 */
	public static final String APPLICATION_NAME = "XX-Application-Name";

	/**
	 * Inidica log de business service.
	 */
	public static final String BUSINESS_SERVICE_LOG = "BUSINESS SERVICE - [Realiza reverso del pago de servicios]";

	/**
	 * Static variable for show curly brackets.
	 */
	public static final String MSG_CURLY_BRACKETS = "{}";

	/**
	 * Static variable for show the message of error response.
	 */
	public static final String MSG_ERROR_RESPONSE_HAS_NO_BODY = "Failed to parse the playload: Response has no body.";

	/**
	 * Static variable for show the message of error format.
	 */
	public static final String MSG_ERROR_FORMAT = "Failed to parse the playload. The format of the message does not "
			+ "correspond with the predefined for ambar {}";

	/**
	 * Static variable for show the message of status.
	 */
	public static final String MSG_STATUS = "status";

	/**
	 * Static variable for show the message microServiceName.
	 */
	public static final String MSG_REQUEST = "request";

	/**
	 * Static variable for show the message of response.
	 */
	public static final String MSG_RESPONSE = "response";

	/**
	 * Static variable for show the message of Error feign details.
	 */
	public static final String ERROR_FEIGN_DETAILS = "Error feign details {}";

	/**
	 * Constant used to represent a comma as separator.
	 */
	public static final String COMMA_SEPARATOR = ", ";

	/**
	 * Constante que contiene el mensaje a logear de getPaymentAgreement.
	 */
	public static final String LOG_REVERSE_TRASNFER = "SERVICE CALL - [Obtiene el reverso de pago]";

	/**
	 * Constant that is used as a key to store the initial time of the request.
	 */
	public static final String T0_REQ_ATTRIBUTE = "req.t0";

	/**
	 * Constant that is used as a key to store the uuid header.
	 */
	public static final String UUID_MDC_LABEL = "mdc.uuid";

	/**
	 * Constant that is used to show a message about the begin of the request.
	 */
	public static final String START_REQUEST = "Inicia Llamado [{}]";

	/**
	 * Constant used to log a header and its value.
	 */
	public static final String MSG_TO_LOG_HEADER = "[{} : {}]";

	/**
	 * Constant that is used to show a message about the total time of the request.
	 */
	public static final String TIME_ELAPSED_MESSAGE = "Time elapsed for request/response roundtrip [{}], {}";

}
