package com.bancoppel.pymt.reverse.transaction.service;

import java.util.concurrent.CompletionException;
import java.util.concurrent.TimeoutException;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

import com.bancoppel.commons.annotation.HandledProcedure;
import com.bancoppel.commons.exceptions.ExternalResourceException;
import com.bancoppel.commons.exceptions.NotValidHeadersException;
import com.bancoppel.pymt.reverse.transaction.constant.ApiConstant;
import com.bancoppel.pymt.reverse.transaction.constant.FeignConstants;
import com.bancoppel.pymt.reverse.transaction.exception.DownstreamException;
import com.bancoppel.pymt.reverse.transaction.model.ApplyThirdPartyTransferRequest;
import com.bancoppel.pymt.reverse.transaction.model.ApplyThirdPartyTransferResponse;

import feign.FeignException;
import feign.RetryableException;

/**
 * Interfaz de tipo Feign que consume el Api Intrabanck transfer
 * 
 * @author DMR
 */
@FeignClient(url = FeignConstants.INTRABANK_TRANSFER_URL, name = FeignConstants.INTRABANK_TRANSFER_NAME)
public interface IintrabankTransferFeign {

	/**
	 * Metodo que consume al Api INTRABANK_TRANSFER.
	 * 
	 * @param authorization              Cabecera Authorization.
	 * @param uuid                       Cabecera uuid.
	 * @param accept                     Cabecera Accept.
	 * @param deviceId                   id de disposiivo.
	 * @param getPaymentAgreementRequest request a api.
	 * @return Respuesta HttpStatus.
	 */
	@HandledProcedure(name = ApiConstant.LOG_REVERSE_TRASNFER, value = ApiConstant.LOG_REVERSE_TRASNFER, ignoreExceptions = {
			DownstreamException.class, FeignException.class, NotValidHeadersException.class, CompletionException.class,
			ExternalResourceException.class, RetryableException.class, TimeoutException.class })
	@PostMapping(value = FeignConstants.INTRABANK_TRANSFER_ENDPOINT)
	ApplyThirdPartyTransferResponse executeTransfer(
			@RequestHeader(name = ApiConstant.ACCEPT) String accept,
			@RequestHeader(name = ApiConstant.AUTHORIZATION) String authorization,
			//@RequestHeader(name = ApiConstant.CONTENT_TYPE) String contentType,
			@RequestHeader(name = ApiConstant.UUID) String uuid,
			@RequestHeader(name = ApiConstant.DATE) String date,
			@RequestHeader(name = ApiConstant.ACCEPT_CHARSET) String acceptCharset,
			@RequestHeader(name = ApiConstant.ACCEPT_ENCODING) String acceptEncoding,
			@RequestHeader(name = ApiConstant.ACCEPT_LANGUAGE) String acceptLanguage,
			@RequestHeader(name = ApiConstant.HOST) String host,
			@RequestHeader(name = ApiConstant.USER_AGENT) String userAgent,
			@RequestHeader(name = ApiConstant.CHANNEL_ID) String channelId,
			@RequestHeader(name = ApiConstant.CONTENT_ENCODING) String contentEncoding,
			@RequestHeader(name = ApiConstant.CONTENT_LANGUAGE) String contentlanguage,
			//@RequestHeader(name = ApiConstant.CONTENT_LENGTH) String contentLength,
			@RequestHeader(name = ApiConstant.CONTENT_MD5) String contentMd5,
			@RequestHeader(name = ApiConstant.DEVICE_ID) String deviceId,
			@RequestBody ApplyThirdPartyTransferRequest applyThirdPartyTransferRequest);
	

}
