package com.bancoppel.pymt.reverse.transaction.exception;

import com.bancoppel.pymt.reverse.transaction.constant.ApiConstant;

import lombok.Getter;

@Getter
public class InvalidServiceException extends RuntimeException {


  /**
   * UID default de la clase.
   */
  private static final long serialVersionUID = 1L;

  /**
   * Constructor por default.
   */
  public InvalidServiceException() {
    super(ApiConstant.EMPTY_STRING);
  }
}
