package com.bancoppel.pymt.reverse.transaction.exception;

import com.bancoppel.pymt.reverse.transaction.constant.ConstantValidate;

import lombok.Getter;

@Getter
public class ReverseNotValidException extends RuntimeException {

	/**
	 * UID default de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor por default.
	 */
	public ReverseNotValidException() {
		super(ConstantValidate.EMPTY_STRING);
	}

}
