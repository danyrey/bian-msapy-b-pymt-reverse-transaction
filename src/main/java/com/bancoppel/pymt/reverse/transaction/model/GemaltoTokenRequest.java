package com.bancoppel.pymt.reverse.transaction.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GemaltoTokenRequest {

	  private String otp;
	  private String customerNumber;
}
