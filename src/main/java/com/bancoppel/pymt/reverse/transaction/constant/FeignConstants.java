package com.bancoppel.pymt.reverse.transaction.constant;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@EnableConfigurationProperties
public class FeignConstants {
	
	  /**
	   * Constante que representa la URL del feign gemaltoToken.
	   */
	  public static final String GEMALTO_TOKEN_URL = "${constants.feign.url.gemaltoToken}";
	  
	  /**
	   * Constante que representa el nombre del feign gemaltoToken.
	   */
	  public static final String GEMALTO_TOKEN_NAME = "${constants.feign.name.gemaltoToken}";
	  
	  /**
	   * Constante que representa el endpoint del feign gemaltoToken.
	   */
	  public static final String GEMALTO_TOKEN_ENDPOINT = "${constants.feign.path.gemaltoToken}";
	  
	  
	  /**
	   * Constante que representa el endpoint del feign getCreditLoansAccounts.
	   */
	  public static final String INTRABANK_TRANSFER_URL = "${constants.feign.url.getIntrabankTransfer}";
	  
	  
	  /**
	   * Constante que representa el nombre del feign gemaltoToken.
	   */
	  public static final String INTRABANK_TRANSFER_NAME = "${constants.feign.name.getIntrabankTransfer}";
	  
	  /**
	   * Constante que representa el endpoint del feign gemaltoToken.
	   */
	  public static final String INTRABANK_TRANSFER_ENDPOINT = "${constants.feign.path.getIntrabankTransfer}";
	  
	  
}
