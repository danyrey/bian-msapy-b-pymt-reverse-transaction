package com.bancoppel.pymt.reverse.transaction.exception;

import lombok.Getter;

@Getter
public class AmountException extends RuntimeException {

  /**
   * Generated serial unique ID.
   */
  private static final long serialVersionUID = -7894187026358558149L;

  private final String code;

  /**
   * Crea una excepcion con su causa y mensaje de error.
   * 
   * @param message Mensaje detallando el error.
   * @param code codigo del error.
   */
  public AmountException(String message, String code) {
    super(message);
    this.code = code;
  }
}
