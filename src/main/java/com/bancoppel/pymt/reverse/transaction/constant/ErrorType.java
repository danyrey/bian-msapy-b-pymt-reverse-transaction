package com.bancoppel.pymt.reverse.transaction.constant;


/**
 * Enum that defines the error types for error responses.
 */
public enum ErrorType {

	ERROR, WARN, INVALID, FATAL;
}
