package com.bancoppel.pymt.reverse.transaction.component;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

import com.bancoppel.pymt.reverse.transaction.constant.ApiConstant;
import com.bancoppel.pymt.reverse.transaction.constant.ApiValues;
import com.bancoppel.pymt.reverse.transaction.constant.ErrorResolverConstant;
import com.bancoppel.pymt.reverse.transaction.exception.DownstreamException;
import com.bancoppel.pymt.reverse.transaction.exception.InvalidGemaltoTokenException;
import com.bancoppel.pymt.reverse.transaction.util.Util;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import feign.Response;
import feign.codec.ErrorDecoder;

public class CustomFeignErrorDecoder implements ErrorDecoder {

	/**
	 * Static variable of record.
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(CustomFeignErrorDecoder.class);

	/**
	 * Static variable for new Error Decoder.
	 */
	private ErrorDecoder errorDecoder = new ErrorDecoder.Default();

	@Autowired
	private ApiValues apiValues;

	/**
	 * Injected bean to retrieve the constants defined in external yaml file.
	 */
	@Autowired
	private ErrorResolverConstant errorResolverConstants;

	/**
	 * Function that response a exception when is invoked.
	 */
	@Override
	public Exception decode(String methodKey, Response response) {

		Map<String, String> errorResponse;

		if (response.request().url().contains(apiValues.getGemaltoToken())) {
			return new InvalidGemaltoTokenException("gemalto token invalido");
		}

		if (response.body() == null) {
			LOGGER.error(ApiConstant.MSG_CURLY_BRACKETS, ApiConstant.MSG_ERROR_RESPONSE_HAS_NO_BODY);
			return errorDecoder.decode(methodKey, response);
		}

		try {
			final String body = feign.Util.toString(response.body().asReader());
			ObjectMapper mapper = new ObjectMapper();
			errorResponse = mapper.readValue(body, new TypeReference<Map<String, String>>() {
			});
		} catch (IOException ex) {
			LOGGER.error(ApiConstant.MSG_ERROR_FORMAT, ex);
			return errorDecoder.decode(methodKey, response);
		}

		HttpStatus status = HttpStatus.valueOf(response.status());

		if (HttpStatus.NOT_FOUND == status) {
			errorResponse.put(ErrorResolverConstant.ERROR_RESPONSE_CODE,
					errorResolverConstants.getDataNotFoundException());
		}

		Map<String, Object> map = new HashMap<>();
		map.put(ApiConstant.MSG_STATUS, status.value());
		map.put(ApiConstant.MSG_REQUEST, Util.getJson(response.request()));
		map.put(ApiConstant.MSG_RESPONSE, Util.getJson(errorResponse));
		String mapString =  Util.getJson(map);
		LOGGER.debug(ApiConstant.ERROR_FEIGN_DETAILS, mapString);

		return new DownstreamException(status.value(), errorResponse);

	}

}
