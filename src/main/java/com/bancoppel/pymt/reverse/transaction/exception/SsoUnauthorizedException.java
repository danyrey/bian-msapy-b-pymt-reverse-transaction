package com.bancoppel.pymt.reverse.transaction.exception;

import com.bancoppel.pymt.reverse.transaction.constant.ApiConstant;

import lombok.Getter;

@Getter
public class SsoUnauthorizedException extends RuntimeException {

  /**
   * UID default de la clase.
   */
  private static final long serialVersionUID = 1L;

  /**
   * Constructor por default.
   */
  public SsoUnauthorizedException() {
    super(ApiConstant.EMPTY_STRING);
  }
}
