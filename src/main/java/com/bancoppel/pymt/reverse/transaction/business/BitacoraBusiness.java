package com.bancoppel.pymt.reverse.transaction.business;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bancoppel.ptsv.log.domain.Log;
import com.bancoppel.ptsv.log.service.ApplicationLogService;
import com.bancoppel.pymt.reverse.transaction.constant.LogValues;
import com.bancoppel.pymt.reverse.transaction.model.Bitacora;

import lombok.extern.slf4j.Slf4j;

/**
 * Clase para guardar bitacora.
 *
 * @author DMR
 */
@Service
@Slf4j
public class BitacoraBusiness {

	/**
	 * Interfaz de dependencia de bitácora.
	 */
	@Autowired
	private ApplicationLogService appLogSvc;

	/**
	 * Clase donde radican las constantes utilizadas para log.
	 */
	@Autowired
	private LogValues logValues;
	
	
	 /**
	   * Método para guardar en bitácora.
	   * 
	   * @param bitacora objeto para llenar peticion a mongo.
	   */
	public void saveBitacora(Bitacora bitacora) {

	    DateTimeFormatter formatter = DateTimeFormatter.ofPattern(LogValues.DATE_FORMATTER);
	    DateTimeFormatter formatterTime = DateTimeFormatter.ofPattern(LogValues.TIME_FORMATTER);

	    Log logB = new Log();

	    logB.setLogType(bitacora.getLogType());
	    logB.setOperationTime(LocalDateTime.now().format(formatterTime));
	    logB.setOperationDate(LocalDateTime.now().format(formatter));
	    logB.setOperationId(bitacora.getOperationId());
	    logB.setBranch(logValues.getBranchId());
	    logB.setUserId(bitacora.getCustomerNumber());
	    logB.setCoreEnvironment(logValues.getCoreEnvironment());
	    logB.setDeviceId(bitacora.getDeviceId());
	    logB.setUuid(bitacora.getUuid());
	    logB.setHttpStatus(String.valueOf(bitacora.getHttpStatus()));
	    logB.setErrorCode(bitacora.getCode());
	    logB.setNamespace(logValues.getNameSpace());
	    logB.setApi(logValues.getPath());

	    if (Objects.nonNull(bitacora.getGenericData())) {
	      logB.setGenericData(bitacora.getGenericData());
	    }

	    if (Objects.nonNull(bitacora.getTransactionLog())) {
	      logB.setTransaction(bitacora.getTransactionLog());
	    }

	    log.debug("[Objeto que se va a guardar en bitacora: {}]", logB);

	    long start = System.currentTimeMillis();
	    appLogSvc.saveOperationalLog(logB);
	    long finish = System.currentTimeMillis();

	    String time = Long.toString(finish - start);

	    if (LogValues.STRING_ZERO_ZERO_ONE.equals(logB.getLogType())) {
	      log.info(LogValues.LOG_OPERATIONAL_SAVE, time);
	    } else {
	      log.info(LogValues.LOG_TRANSACTIONAL_SAVE, time);
	    }



	  }

}
