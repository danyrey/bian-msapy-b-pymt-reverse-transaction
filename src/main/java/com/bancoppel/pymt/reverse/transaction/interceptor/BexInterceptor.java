package com.bancoppel.pymt.reverse.transaction.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.bancoppel.pymt.reverse.transaction.constant.ApiConstant;
import com.bancoppel.pymt.reverse.transaction.util.Util;

import lombok.extern.slf4j.Slf4j;

/**
 * Class that defines methods to be executed when intercepting requests.
 */
@Slf4j
@Component
public class BexInterceptor extends HandlerInterceptorAdapter {

  @Value("${constants.api.headers.print}")
  private boolean printHeaders;
  
  /**
   * Call the service to validate the session and establish in context the client and the
   * representative in the session, and UUID headers, as well as start establishing the
   * start of the total time of the request.
   */
  @Override
  public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
      throws ServletRequestBindingException {
    long t0 = System.currentTimeMillis();
    request.setAttribute(ApiConstant.T0_REQ_ATTRIBUTE, t0);

    MDC.put(ApiConstant.UUID_MDC_LABEL, request.getHeader(ApiConstant.UUID));
    log.info(ApiConstant.START_REQUEST, request.getRequestURI());

    if (printHeaders) {
      Util.printHeaders(request);
    }

    return true;
  }

  /**
   * Calculate the total time of the request and clean the data of the MDC.
   */
  @Override
  public void afterCompletion(HttpServletRequest request, HttpServletResponse response,
      Object handler, Exception ex) {
    long t0 = (Long) request.getAttribute(ApiConstant.T0_REQ_ATTRIBUTE);
    long timeElapsed = System.currentTimeMillis() - t0;
    log.info(ApiConstant.TIME_ELAPSED_MESSAGE, request.getRequestURI(), timeElapsed);

    request.removeAttribute(ApiConstant.T0_REQ_ATTRIBUTE);

    MDC.clear();
  }
}
