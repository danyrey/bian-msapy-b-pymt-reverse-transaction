/***********************************************************************
 * Copyright (c) 2023 Kairosds S.A. de C.V. All rights reserved.
 *
 * Licensed under the GNU General Public License, Version 3 (the 
 * "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at
 *
 *   https://www.gnu.org/licenses/gpl-3.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or 
 * implied.
 * 
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **********************************************************************/
package com.bancoppel.pymt.reverse.transaction.api;

import com.bancoppel.commons.annotation.ValidateHeaders;
import com.bancoppel.pymt.reverse.transaction.constant.ApiConstant;
import com.bancoppel.pymt.reverse.transaction.constant.ConstantSwagger;
import com.bancoppel.pymt.reverse.transaction.model.RequestReverse;
import com.bancoppel.pymt.reverse.transaction.model.ResponseReverse;
import com.bancoppel.pymt.reverse.transaction.service.IReverseTransaction;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controlador REST para llamadas al servicio de reversos IReverseTransaction.
 * 
 * @author dmr
 *
 */
@RestController
@Validated
@RequestMapping(path = ApiConstant.BASE_PATH)
public class ReverseTransactionController {

	@Autowired
	private IReverseTransaction servicesReverce;

	/**
	 * Metodo que ejecuta logica de negocio para consultar el microservicio de envío
	 * intrabancario.
	 * 
	 * @param request de api.
	 * @return objeto ResponseReverse.
	 */
	@ApiOperation(value = ApiConstant.OPERATION_API, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, tags = {
			ApiConstant.PAYMENT_VALIDATION_TAG })
	@ApiImplicitParams({
//			@ApiImplicitParam(required = true, paramType = ConstantSwagger.HEADER, name = ConstantSwagger.ACCEPT, value = ConstantSwagger.ACCEPT_HEADER_DESCRIPTION, dataType = ConstantSwagger.STRING_TYPE),
//			@ApiImplicitParam(required = true, paramType = ConstantSwagger.HEADER, name = ConstantSwagger.AUTHORIZATION, value = ConstantSwagger.AUTHORIZATION_HEADER_DESCRIPTION, dataType = ConstantSwagger.STRING_TYPE),
//			@ApiImplicitParam(required = true, paramType = ConstantSwagger.HEADER, name = ConstantSwagger.CONTENT_TYPE, value = ConstantSwagger.CONTENT_TYPE_HEADER_DESCRIPTION, dataType = ConstantSwagger.STRING_TYPE),
//			@ApiImplicitParam(required = true, paramType = ConstantSwagger.HEADER, name = ConstantSwagger.UUID, value = ConstantSwagger.UUID_HEADER_DESCRIPTION, dataType = ConstantSwagger.STRING_TYPE),
//			@ApiImplicitParam(required = false, paramType = ConstantSwagger.HEADER, name = ConstantSwagger.DATE, value = ConstantSwagger.DATE_HEADER_DESCRIPTION, dataType = ConstantSwagger.STRING_TYPE),
//			@ApiImplicitParam(required = false, paramType = ConstantSwagger.HEADER, name = ConstantSwagger.ACCEPT_CHARSET, value = ConstantSwagger.ACCEPT_CHARSET_HEADER_DESCRIPTION, dataType = ConstantSwagger.STRING_TYPE),
//			@ApiImplicitParam(required = false, paramType = ConstantSwagger.HEADER, name = ConstantSwagger.ACCEPT_ENCODING, value = ConstantSwagger.ACCEPT_ENCODING_HEADER_DESCRIPTION, dataType = ConstantSwagger.STRING_TYPE),
//			@ApiImplicitParam(required = false, paramType = ConstantSwagger.HEADER, name = ConstantSwagger.ACCEPT_LANGUAGE, value = ConstantSwagger.ACCEPT_LANGUAGE_HEADER_DESCRIPTION, dataType = ConstantSwagger.STRING_TYPE),
//			@ApiImplicitParam(required = false, paramType = ConstantSwagger.HEADER, name = ConstantSwagger.HOST, value = ConstantSwagger.HOST_HEADER_DESCRIPTION, dataType = ConstantSwagger.STRING_TYPE),
//			@ApiImplicitParam(required = false, paramType = ConstantSwagger.HEADER, name = ConstantSwagger.USER_AGENT, value = ConstantSwagger.USER_AGENT_HEADER_DESCRIPTION, dataType = ConstantSwagger.STRING_TYPE),
//			@ApiImplicitParam(required = true, paramType = ConstantSwagger.HEADER, name = ConstantSwagger.CHANNEL_ID, value = ConstantSwagger.CHANNEL_ID_DESCRIPTION, dataType = ConstantSwagger.STRING_TYPE),
//			@ApiImplicitParam(required = false, paramType = ConstantSwagger.HEADER, name = ConstantSwagger.CONTENT_ENCODING, value = ConstantSwagger.CONTENT_ENCODING_HEADER_DESCRIPTION, dataType = ConstantSwagger.STRING_TYPE),
//			@ApiImplicitParam(required = false, paramType = ConstantSwagger.HEADER, name = ConstantSwagger.CONTENT_LANGUAGE, value = ConstantSwagger.CONTENT_LANGUAGE_HEADER_DESCRIPTION, dataType = ConstantSwagger.STRING_TYPE),
//			@ApiImplicitParam(required = false, paramType = ConstantSwagger.HEADER, name = ConstantSwagger.CONTENT_LENGTH, value = ConstantSwagger.CONTENT_LENGTH_HEADER_DESCRIPTION, dataType = ConstantSwagger.STRING_TYPE),
//			@ApiImplicitParam(required = false, paramType = ConstantSwagger.HEADER, name = ConstantSwagger.CONTENT_MD5, value = ConstantSwagger.CONTENT_MD5_HEADER_DESCRIPTION, dataType = ConstantSwagger.STRING_TYPE),
//			@ApiImplicitParam(required = true, paramType = ConstantSwagger.HEADER, name = ConstantSwagger.DEVICE_ID, value = ConstantSwagger.DEVICE_ID_DESCRIPTION, dataType = ConstantSwagger.STRING_TYPE)})
		@ApiImplicitParam(required = false, paramType = ConstantSwagger.HEADER, name = ConstantSwagger.ACCEPT, value = ConstantSwagger.ACCEPT_HEADER_DESCRIPTION, dataType = ConstantSwagger.STRING_TYPE),
		@ApiImplicitParam(required = false, paramType = ConstantSwagger.HEADER, name = ConstantSwagger.AUTHORIZATION, value = ConstantSwagger.AUTHORIZATION_HEADER_DESCRIPTION, dataType = ConstantSwagger.STRING_TYPE),
		@ApiImplicitParam(required = false, paramType = ConstantSwagger.HEADER, name = ConstantSwagger.CONTENT_TYPE, value = ConstantSwagger.CONTENT_TYPE_HEADER_DESCRIPTION, dataType = ConstantSwagger.STRING_TYPE),
		@ApiImplicitParam(required = false, paramType = ConstantSwagger.HEADER, name = ConstantSwagger.UUID, value = ConstantSwagger.UUID_HEADER_DESCRIPTION, dataType = ConstantSwagger.STRING_TYPE),
		@ApiImplicitParam(required = false, paramType = ConstantSwagger.HEADER, name = ConstantSwagger.DATE, value = ConstantSwagger.DATE_HEADER_DESCRIPTION, dataType = ConstantSwagger.STRING_TYPE),
		@ApiImplicitParam(required = false, paramType = ConstantSwagger.HEADER, name = ConstantSwagger.ACCEPT_CHARSET, value = ConstantSwagger.ACCEPT_CHARSET_HEADER_DESCRIPTION, dataType = ConstantSwagger.STRING_TYPE),
		@ApiImplicitParam(required = false, paramType = ConstantSwagger.HEADER, name = ConstantSwagger.ACCEPT_ENCODING, value = ConstantSwagger.ACCEPT_ENCODING_HEADER_DESCRIPTION, dataType = ConstantSwagger.STRING_TYPE),
		@ApiImplicitParam(required = false, paramType = ConstantSwagger.HEADER, name = ConstantSwagger.ACCEPT_LANGUAGE, value = ConstantSwagger.ACCEPT_LANGUAGE_HEADER_DESCRIPTION, dataType = ConstantSwagger.STRING_TYPE),
		@ApiImplicitParam(required = false, paramType = ConstantSwagger.HEADER, name = ConstantSwagger.HOST, value = ConstantSwagger.HOST_HEADER_DESCRIPTION, dataType = ConstantSwagger.STRING_TYPE),
		@ApiImplicitParam(required = false, paramType = ConstantSwagger.HEADER, name = ConstantSwagger.USER_AGENT, value = ConstantSwagger.USER_AGENT_HEADER_DESCRIPTION, dataType = ConstantSwagger.STRING_TYPE),
		@ApiImplicitParam(required = false, paramType = ConstantSwagger.HEADER, name = ConstantSwagger.CHANNEL_ID, value = ConstantSwagger.CHANNEL_ID_DESCRIPTION, dataType = ConstantSwagger.STRING_TYPE),
		@ApiImplicitParam(required = false, paramType = ConstantSwagger.HEADER, name = ConstantSwagger.CONTENT_ENCODING, value = ConstantSwagger.CONTENT_ENCODING_HEADER_DESCRIPTION, dataType = ConstantSwagger.STRING_TYPE),
		@ApiImplicitParam(required = false, paramType = ConstantSwagger.HEADER, name = ConstantSwagger.CONTENT_LANGUAGE, value = ConstantSwagger.CONTENT_LANGUAGE_HEADER_DESCRIPTION, dataType = ConstantSwagger.STRING_TYPE),
		@ApiImplicitParam(required = false, paramType = ConstantSwagger.HEADER, name = ConstantSwagger.CONTENT_LENGTH, value = ConstantSwagger.CONTENT_LENGTH_HEADER_DESCRIPTION, dataType = ConstantSwagger.STRING_TYPE),
		@ApiImplicitParam(required = false, paramType = ConstantSwagger.HEADER, name = ConstantSwagger.CONTENT_MD5, value = ConstantSwagger.CONTENT_MD5_HEADER_DESCRIPTION, dataType = ConstantSwagger.STRING_TYPE),
		@ApiImplicitParam(required = false, paramType = ConstantSwagger.HEADER, name = ConstantSwagger.DEVICE_ID, value = ConstantSwagger.DEVICE_ID_DESCRIPTION, dataType = ConstantSwagger.STRING_TYPE)})
	@ApiResponses(value = {
			@ApiResponse(code = ConstantSwagger.CODE_OK, message = ConstantSwagger.OK, response = ResponseReverse.class),
			@ApiResponse(code = ConstantSwagger.CODE_BAD_REQUEST, message = ConstantSwagger.BAD_REQUEST),
			@ApiResponse(code = ConstantSwagger.CODE_RESOURCE_NOT_FOUND, message = ConstantSwagger.RESOURCE_NOT_FOUND),
			@ApiResponse(code = ConstantSwagger.CODE_UNAUTHORIZED, message = ConstantSwagger.UNAUTHORIZED),
			@ApiResponse(code = ConstantSwagger.CODE_INTERNAL_ERROR, message = ConstantSwagger.INTERNAL_ERROR)
		
		 })
	@ValidateHeaders
	@PostMapping(value = ApiConstant.API_ENDPOINT_SERVICE_PAYMENT , consumes = MediaType.APPLICATION_JSON_VALUE , produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseReverse> reverseTransaction(@RequestHeader @ApiIgnore HttpHeaders httpHeaders,
			@RequestBody @Valid RequestReverse requestReverse) {
		return new ResponseEntity<>(servicesReverce.reverseTransactionService(requestReverse, httpHeaders), HttpStatus.OK);

	}

}