package com.bancoppel.pymt.reverse.transaction.constant;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Constantes de api.
 *
 * @author dmr
 */
@Component
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ApiValues {

	/**
	 * Nombre de campo en request.
	 */
	public static final String SBCAMOUNT = "SBCAmount";

	/**
	 * Nombre de campo en request.
	 */
	public static final String REMAMOUNT = "RemAmount";

	/**
	 * Nombre de campo en request.
	 */
	public static final String RETDAYS = "RetDays";

	/**
	 * Variable que representa.
	 */
	@Value("${constants.apiValues.values.validLongAccount}")
	private int validLongAccount;

	/**
	 * Variable que representa.
	 */
	@Value("${constants.feign.path.gemaltoToken}")
	private String gemaltoToken;

	/**
	 * Variable que define si el path va en location.
	 */
	@Value("${api.enable.show-details}")
	private boolean isDetailsDefault;

	/**
	 * Etiqueta de aplicación.
	 */
	@Value("${constants.swagger.label}")
	private String label;

	/**
	 * Aplicación resourceLocation.
	 */
	@Value("${constants.swagger.resourceLocation}")
	private String resourceLocation;

	/**
	 * Applicación webjars.
	 */
	@Value("${constants.swagger.webjars}")
	private String webjars;

	/**
	 * Applicación webjarsLocation.
	 */
	@Value("${constants.swagger.webjarsLocation}")
	private String webjarsLocation;

	/**
	 * Patrón de ruta constante para el cierre de sesión del interceptor.
	 */
	@Value("${constants.api.uri.interceptorPath}")
	private String interceptorPath;

}
