package com.bancoppel.pymt.reverse.transaction.component;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeoutException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.NoHandlerFoundException;

import com.bancoppel.commons.exceptions.NotValidHeadersException;
//import com.bancoppel.decrypt.exceptions.CryptographiSecurityException;
import com.bancoppel.it.management.sso.exception.InvalidAttemptException;
import com.bancoppel.it.management.sso.exception.InvalidSsoExeption;
import com.bancoppel.pymt.reverse.transaction.constant.ApiConstant;
import com.bancoppel.pymt.reverse.transaction.constant.ApiValues;
import com.bancoppel.pymt.reverse.transaction.constant.ErrorResolverConstant;
import com.bancoppel.pymt.reverse.transaction.constant.ErrorType;
import com.bancoppel.pymt.reverse.transaction.exception.AmountException;
import com.bancoppel.pymt.reverse.transaction.exception.BusinessException;
import com.bancoppel.pymt.reverse.transaction.exception.DownstreamException;
import com.bancoppel.pymt.reverse.transaction.exception.ForbiddenException;
import com.bancoppel.pymt.reverse.transaction.exception.InvalidGemaltoTokenException;
import com.bancoppel.pymt.reverse.transaction.exception.InvalidServiceException;
import com.bancoppel.pymt.reverse.transaction.exception.PaymentErrorException;
import com.bancoppel.pymt.reverse.transaction.exception.RedisNotValidException;
import com.bancoppel.pymt.reverse.transaction.exception.ReverseNotValidException;
import com.bancoppel.pymt.reverse.transaction.exception.SsoUnauthorizedException;
import com.bancoppel.pymt.reverse.transaction.model.ErrorResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.netflix.hystrix.exception.HystrixRuntimeException;

import feign.RetryableException;
import lombok.extern.slf4j.Slf4j;

/**
 * Clase para controlar el manejo de errores.
 */
@Slf4j
@RestControllerAdvice
public class ErrorResolver {

	/**
	 * Inyeccion de valores de api.
	 */
	@Autowired
	private ApiValues apiValues;

	/**
	 * Injected bean to retrieve the constants defined in external yaml file.
	 */
	@Autowired
	private ErrorResolverConstant errorResolverConstants;

	/**
	 * Metodo para escribir el detalle del error en el log.
	 */
	private static void writeToLog(ErrorResponse errorResponse, Exception exception) {
		log.error(ErrorResolverConstant.ERROR_RESPONSE_TYPE_INFO, errorResponse.getType());
		log.error(ErrorResolverConstant.ERROR_RESPONSE_CODE_INFO, errorResponse.getCode());
		log.error(ErrorResolverConstant.ERROR_RESPONSE_DETAILS_INFO, errorResponse.getDetails());
		log.error(ErrorResolverConstant.ERROR_RESPONSE_LOCATION_INFO, errorResponse.getLocation());
		log.error(ErrorResolverConstant.ERROR_RESPONSE_MORE_INFORMATION_INFO, errorResponse.getMoreInfo());

		String message = Objects.isNull(exception) ? ApiConstant.EMPTY_STRING : exception.getMessage();
		log.error(message, exception);
	}

	/**
	 * DownstreamException handler.
	 *
	 * @param req  Http Servlet Request.
	 * @param resp Http Servlet Response.
	 * @param ex   DownstreamException object.
	 * @return Error Response for DownstreamException.
	 */
	@ExceptionHandler(DownstreamException.class)
	@ResponseBody
	public ErrorResponse resolveDownstreamException(HttpServletRequest req, HttpServletResponse resp,
			DownstreamException ex) {
		log.error(ex.getMessage(), ex);

		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setType(ex.getType());
		errorResponse.setCode(ex.getCode());
		errorResponse.setDetails(ex.getDetails());
		errorResponse.setLocation(req.getRequestURI());
		errorResponse.setMoreInfo(ex.getMoreInfo());
		errorResponse.setTimestamp(ex.getTimestamp());
		errorResponse.setUuid(ex.getUuid());
		resp.setStatus(ex.getStatus());

		ErrorResolver.writeToLog(errorResponse, ex);

		return errorResponse;
	}

	/**
	 * Handler for NoHandlerFoundException.
	 *
	 * @param req of type HttpServletRequest.
	 * @param ex  of type NoHandlerFoundException.
	 * @return errorResponse object.
	 */
	@ExceptionHandler(NoHandlerFoundException.class)
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	public ErrorResponse resolveNoHandlerFoundException(HttpServletRequest req, NoHandlerFoundException ex) {
		ErrorResponse errorResponse = new ErrorResponse();
		log.debug(ex.getMessage());
		errorResponse.setType(ErrorType.ERROR.name());
		errorResponse.setCode(errorResolverConstants.getNoHandlerFoundException());
		errorResponse.setDetails(errorResolverConstants.getMensajeDefaultError());
		errorResponse.setTimestamp(ZonedDateTime.now());
		errorResponse.setUuid(req.getHeader(ApiConstant.UUID));
		errorResponse.setLocation(req.getRequestURI());

		ErrorResolver.writeToLog(errorResponse, ex);

		return errorResponse;
	}

	/**
	 * Handler for HttpRequestMethodNotSupportedException.
	 *
	 * @param req of type HttpServletRequest.
	 * @param ex  of type HttpRequestMethodNotSupportedException.
	 * @return errorResponse object.
	 */
	@ExceptionHandler(HttpRequestMethodNotSupportedException.class)
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public ErrorResponse resolveHttpRequestMethodNotSupportedException(HttpServletRequest req,
			HttpRequestMethodNotSupportedException ex) {
		ErrorResponse errorResponse = new ErrorResponse();
		log.debug(ex.getMessage());
		errorResponse.setType(ErrorType.INVALID.name());
		errorResponse.setCode(errorResolverConstants.getHttpRequestMethodNotSupportedException());
		errorResponse.setDetails(errorResolverConstants.getMensajeDefaultError());
		errorResponse.setMoreInfo(req.getRequestURI());
		errorResponse.setTimestamp(ZonedDateTime.now());
		errorResponse.setUuid(req.getHeader(ApiConstant.UUID));
		errorResponse.setLocation(req.getRequestURI());

		ErrorResolver.writeToLog(errorResponse, ex);

		return errorResponse;
	}

	/**
	 * Handler for HttpMediaTypeNotAcceptableException. See
	 * https://www.baeldung.com/spring-httpmediatypenotacceptable
	 *
	 * @param req The httpRequest as in the Servlet API.
	 * @param ex  of type HttpMediaTypeNotAcceptableException.
	 * @return errorResponse object.
	 */
	@ExceptionHandler(HttpMediaTypeNotAcceptableException.class)
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public String resolveHttpMediaTypeNotAcceptableException(HttpServletRequest req,
			HttpMediaTypeNotAcceptableException ex) {
		ObjectMapper mapper = new ObjectMapper();

		ErrorResponse errorResponse = new ErrorResponse();

		errorResponse.setType(ErrorType.INVALID.name());
		errorResponse.setCode(errorResolverConstants.getHttpMediaTypeNotAcceptableException());
		errorResponse.setDetails(ex.getMessage());
		errorResponse.setTimestamp(ZonedDateTime.now());
		errorResponse.setUuid(req.getHeader(ApiConstant.UUID));
		errorResponse.setLocation(req.getRequestURI());

		ErrorResolver.writeToLog(errorResponse, ex);

		String errorResponseString = ex.getMessage();

		try {
			errorResponseString = mapper.writeValueAsString(errorResponse);
		} catch (JsonProcessingException jsonProcessingException) {
			log.error(jsonProcessingException.getMessage(), jsonProcessingException);
		}

		return errorResponseString;
	}

	/**
	 * Handler for HttpMediaTypeNotSupportedException.
	 *
	 * @param req The httpRequest as in the Servlet API.
	 * @param ex  of type HttpMediaTypeNotSupportedException.
	 * @return errorResponse object.
	 */
	@ExceptionHandler(HttpMediaTypeNotSupportedException.class)
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public ErrorResponse resolveHttpMediaTypeNotSupportedException(HttpServletRequest req,
			HttpMediaTypeNotSupportedException ex) {
		ErrorResponse errorResponse = new ErrorResponse();

		errorResponse.setType(ErrorType.INVALID.name());
		errorResponse.setCode(errorResolverConstants.getHttpMediaTypeNotSupportedException());
		errorResponse.setDetails(ex.getMessage());
		errorResponse.setTimestamp(ZonedDateTime.now());
		errorResponse.setUuid(req.getHeader(ApiConstant.UUID));
		errorResponse.setLocation(req.getRequestURI());

		ErrorResolver.writeToLog(errorResponse, ex);

		return errorResponse;
	}

	/**
	 * Handler for ServletRequestBindingException.
	 *
	 * @param req The httpRequest as in the Servlet API.
	 * @param ex  of type ServletRequestBindingException.
	 * @return errorResponse object.
	 */
	@ExceptionHandler(ServletRequestBindingException.class)
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public ErrorResponse resolveServletRequestBindingException(HttpServletRequest req,
			ServletRequestBindingException ex) {
		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setType(ErrorType.ERROR.name());
		errorResponse.setCode(errorResolverConstants.getServletRequestBindingException());
		errorResponse.setDetails(ex.getMessage());
		errorResponse.setTimestamp(ZonedDateTime.now());
		errorResponse.setUuid(req.getHeader(ApiConstant.UUID));
		errorResponse.setLocation(req.getRequestURI());

		ErrorResolver.writeToLog(errorResponse, ex);

		return errorResponse;
	}

	/**
	 * Handler for HttpMessageNotReadableException.
	 *
	 * @param req HttpServletRequest param.
	 * @param ex  of type HttpMessageNotReadableException.
	 * @return errorResponse object.
	 */
	@ExceptionHandler(HttpMessageNotReadableException.class)
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public ErrorResponse resolveHttpMessageNotReadableException(HttpServletRequest req,
			HttpMessageNotReadableException ex) {
		ErrorResponse errorResponse = new ErrorResponse();

		String message = Objects.isNull(ex) ? ApiConstant.EMPTY_STRING : ex.getMessage();
		message = Objects.isNull(message) ? ApiConstant.EMPTY_STRING : message;

		int index = message.indexOf(ApiConstant.COLON);
		message = (index != ApiConstant.INT_NEGATIVE_ONE) ? message.substring(ApiConstant.INT_ZERO_VALUE, index)
				: errorResolverConstants.getGenericErrorDescription();

		errorResponse.setType(ErrorType.INVALID.name());
		errorResponse.setCode(errorResolverConstants.getHttpMessageNotReadableException());
		errorResponse.setDetails(message);
		errorResponse.setTimestamp(ZonedDateTime.now());
		errorResponse.setUuid(req.getHeader(ApiConstant.UUID));
		errorResponse.setLocation(req.getRequestURI());

		ErrorResolver.writeToLog(errorResponse, ex);

		return errorResponse;
	}

	/**
	 * Handler for MethodArgumentNotValidException.
	 *
	 * @param req of type HttpServletRequest.
	 * @param ex  of type MethodArgumentNotValidException.
	 * @return errorResponse object.
	 */
	@ExceptionHandler(MethodArgumentNotValidException.class)
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public ErrorResponse resolveMethodArgumentNotValidException(HttpServletRequest req,
			MethodArgumentNotValidException ex) {
		ErrorResponse errorResponse = new ErrorResponse();

		errorResponse.setType(ErrorType.INVALID.name());
		errorResponse.setCode(errorResolverConstants.getMethodArgumentNotValidException());

		Map<String, List<String>> groupedErrors = new HashMap<>();
		List<String> fields = new ArrayList<>();
		List<FieldError> fieldErrors = ex.getBindingResult().getFieldErrors();
		for (FieldError fieldError : fieldErrors) {
			String message = fieldError.getDefaultMessage();
			String field = fieldError.getField();
			groupedErrors.computeIfAbsent(message, key -> Collections.singletonList(field));
			fields.add(field);
		}

		String details = !groupedErrors.isEmpty() ? groupedErrors.toString() : fields.toString();

		errorResponse.setDetails(details);
		errorResponse.setLocation(req.getRequestURI());
		errorResponse.setTimestamp(ZonedDateTime.now());
		errorResponse.setUuid(req.getHeader(ApiConstant.UUID));

		ErrorResolver.writeToLog(errorResponse, ex);

		return errorResponse;
	}

	/**
	 * Handler for ConstraintViolationException.
	 *
	 * @param req HttpServletRequest.
	 * @param ex  of type ConstraintViolationException.
	 * @return errorResponse object.
	 */
	@ExceptionHandler(ConstraintViolationException.class)
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public ErrorResponse resolveConstraintViolation(HttpServletRequest req, ConstraintViolationException ex) {
		ErrorResponse errorResponse = new ErrorResponse();

		errorResponse.setType(ErrorType.INVALID.name());
		errorResponse.setCode(errorResolverConstants.getConstraintViolationException());

		List<String> violationMessages = new ArrayList<>();
		ex.getConstraintViolations().forEach(violation -> violationMessages.add(violation.getMessage()));

		errorResponse.setDetails(String.join(ApiConstant.COMMA_SEPARATOR, violationMessages));
		errorResponse.setTimestamp(ZonedDateTime.now());
		errorResponse.setUuid(req.getHeader(ApiConstant.UUID));
		errorResponse.setLocation(req.getRequestURI());

		ErrorResolver.writeToLog(errorResponse, ex);

		return errorResponse;
	}

	/**
	 * Handler for Exception.
	 *
	 * @param req of type HttpServletRequest.
	 * @param ex  of type Exception.
	 * @return errorResponse object.
	 */
	@ExceptionHandler(Exception.class)
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	public ErrorResponse resolveException(HttpServletRequest req, Exception ex) {
		ErrorResponse errorResponse = new ErrorResponse();

		errorResponse.setType(ErrorType.FATAL.name());
		errorResponse.setCode(errorResolverConstants.getException());
		errorResponse.setDetails(errorResolverConstants.getMensajeDefaultError());
		errorResponse.setTimestamp(ZonedDateTime.now());
		errorResponse.setUuid(req.getHeader(ApiConstant.UUID));
		errorResponse.setLocation(req.getRequestURI());

		ErrorResolver.writeToLog(errorResponse, ex);

		return errorResponse;
	}

	/**
	 * Handler for HystrixRuntimeException.
	 *
	 * @param req HttpServletRequest param.
	 * @param ex  of type HystrixRuntimeException.
	 * @return errorResponse object.
	 */
	@ExceptionHandler(HystrixRuntimeException.class)
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	public ErrorResponse resolveHystrixRuntimeException(HttpServletRequest req, HystrixRuntimeException ex) {
		ErrorResponse errorResponse = new ErrorResponse();

		errorResponse.setType(ErrorType.ERROR.name());
		errorResponse.setCode(errorResolverConstants.getHystrixRuntimeException());
		errorResponse.setDetails(ex.getMessage());
		errorResponse.setTimestamp(ZonedDateTime.now());
		errorResponse.setUuid(req.getHeader(ApiConstant.UUID));
		errorResponse.setLocation(req.getRequestURI());

		ErrorResolver.writeToLog(errorResponse, ex);

		return errorResponse;
	}

	/**
	 * Obtiene la lista de headers faltantes en la petición, de acuerdo a la
	 * validación realizada por la dependencia.
	 * 
	 * @param req Objeto de la petición
	 * @param ex  Excepción obtenida
	 * @return Lista de Headers faltantes, de acuerdo a validación de dependencia
	 */
	@ExceptionHandler(NotValidHeadersException.class)
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public ErrorResponse resolveNotValidHeadersException(HttpServletRequest req, NotValidHeadersException ex) {
		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setDetails(errorResolverConstants.getGenericErrorDescription());

		errorResponse.setType(ErrorType.ERROR.name());
		errorResponse.setCode(errorResolverConstants.getBadRequestException());
		errorResponse.setDetails(ex.getMessage());
		errorResponse.setTimestamp(ZonedDateTime.now());
		errorResponse.setUuid(req.getHeader(ApiConstant.UUID));
		errorResponse.setLocation(req.getRequestURI());

		ErrorResolver.writeToLog(errorResponse, ex);
		return errorResponse;
	}

	/**
	 * Maneja la excepcion ForbiddenException.
	 * 
	 * @param req Objeto Http Servlet de petición.
	 * @param ex  Excepción recibida ForbiddenException.
	 * @return errorResponse Objeto de respuesta específica para ForbiddenException.
	 */
	@ExceptionHandler(ForbiddenException.class)
	@ResponseStatus(value = HttpStatus.FORBIDDEN)
	public ErrorResponse resolveForbiddenException(HttpServletRequest req, ForbiddenException ex) {

		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setType(ErrorType.ERROR.name());
		errorResponse.setDetails(ex.getMessage());
		errorResponse.setTimestamp(ZonedDateTime.now());
		errorResponse.setUuid(req.getHeader(ApiConstant.UUID));
		errorResponse.setCode(ex.getCode());
		errorResponse.setLocation(req.getRequestURI());

		ErrorResolver.writeToLog(errorResponse, ex);

		return errorResponse;
	}

	/**
	 * Maneja la excepcion BusinessException.
	 * 
	 * @param req Objeto Http Servlet de petición.
	 * @param ex  Excepción recibida BusinessException.
	 * @return errorResponse Objeto de respuesta específica para BusinessException.
	 */
	@ExceptionHandler(BusinessException.class)
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	public ErrorResponse resolveBusinessException(HttpServletRequest req, BusinessException ex) {

		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setType(ErrorType.ERROR.name());
		errorResponse.setDetails(ex.getMessage());
		errorResponse.setTimestamp(ZonedDateTime.now());
		errorResponse.setUuid(req.getHeader(ApiConstant.UUID));
		errorResponse.setCode(errorResolverConstants.getException());
		errorResponse.setLocation(req.getRequestURI());

		ErrorResolver.writeToLog(errorResponse, ex);

		return errorResponse;
	}

	/**
	 * Maneja la excepcion AmountException.
	 * 
	 * @param req Objeto Http Servlet de petición.
	 * @param ex  Excepción recibida AmountException.
	 * @return errorResponse Objeto de respuesta específica para AmountException.
	 */
	@ExceptionHandler(AmountException.class)
	@ResponseStatus(value = HttpStatus.FORBIDDEN)
	public ErrorResponse resolveAmountException(HttpServletRequest req, AmountException ex) {

		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setType(ErrorType.INVALID.name());
		errorResponse.setDetails(ex.getMessage());
		errorResponse.setTimestamp(ZonedDateTime.now());
		errorResponse.setUuid(req.getHeader(ApiConstant.UUID));
		errorResponse.setCode(ex.getCode());
		errorResponse.setLocation(req.getRequestURI());

		ErrorResolver.writeToLog(errorResponse, ex);

		return errorResponse;
	}

	/**
	 * Maneja la excepcion InvalidServiceException.
	 * 
	 * @param req Objeto Http Servlet de petición.
	 * @param ex  Excepción recibida InvalidServiceException.
	 * @return errorResponse Objeto de respuesta específica para
	 *         InvalidServiceException.
	 */
	@ExceptionHandler(InvalidServiceException.class)
	@ResponseStatus(value = HttpStatus.FORBIDDEN)
	public ErrorResponse resolveInvalidServiceException(HttpServletRequest req, InvalidServiceException ex) {

		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setType(ErrorType.INVALID.name());
		errorResponse.setDetails(errorResolverConstants.getMensajeServicioNoDisponible());
		errorResponse.setTimestamp(ZonedDateTime.now());
		errorResponse.setUuid(req.getHeader(ApiConstant.UUID));
		errorResponse.setCode(errorResolverConstants.getCodigoServicioNoDisponible());
		errorResponse.setLocation(req.getRequestURI());

		ErrorResolver.writeToLog(errorResponse, ex);

		return errorResponse;
	}

	/**
	 * Maneja la excepcion PaymentErrorException.
	 * 
	 * @param req Objeto Http Servlet de petición.
	 * @param ex  Excepción recibida PaymentErrorException.
	 * @return errorResponse Objeto de respuesta específica para
	 *         PaymentErrorException.
	 */
	@ExceptionHandler(PaymentErrorException.class)
	@ResponseStatus(value = HttpStatus.FORBIDDEN)
	public ErrorResponse resolvePaymentErrorException(HttpServletRequest req, PaymentErrorException ex) {

		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setType(ErrorType.INVALID.name());
		String details = apiValues.isDetailsDefault() ? errorResolverConstants.getMensajeErrorEnPago()
				: errorResolverConstants.getMensajeDefaultError();
		errorResponse.setDetails(details);
		errorResponse.setTimestamp(ZonedDateTime.now());
		errorResponse.setUuid(req.getHeader(ApiConstant.UUID));
		errorResponse.setCode(errorResolverConstants.getCodigoErrorEnPago());
		errorResponse.setLocation(req.getRequestURI());

		ErrorResolver.writeToLog(errorResponse, ex);

		return errorResponse;
	}

	/**
	 * Maneja la excepcion InvalidGemaltoTokenException.
	 * 
	 * @param req Objeto Http Servlet de petición.
	 * @param ex  Excepción recibida InvalidGemaltoTokenException.
	 * @return errorResponse Objeto de respuesta específica para
	 *         InvalidGemaltoTokenException.
	 */
	@ExceptionHandler(InvalidGemaltoTokenException.class)
	@ResponseStatus(value = HttpStatus.FORBIDDEN)
	public ErrorResponse resolveInvalidGemaltoTokenException(HttpServletRequest req, InvalidGemaltoTokenException ex) {
		log.debug(errorResolverConstants.getMensajeGemaltoTokenInvalido());

		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setType(ErrorType.INVALID.name());
		errorResponse.setDetails(errorResolverConstants.getMensajeDefaultError());
		errorResponse.setTimestamp(ZonedDateTime.now());
		errorResponse.setUuid(req.getHeader(ApiConstant.UUID));
		errorResponse.setCode(errorResolverConstants.getCodigoGemaltoTokenInvalido());
		errorResponse.setLocation(req.getRequestURI());

		ErrorResolver.writeToLog(errorResponse, ex);

		return errorResponse;
	}

	/**
	 * Maneja la excepcion SsoUnauthorizedException.
	 * 
	 * @param req Objeto Http Servlet de petición.
	 * @param ex  Excepción recibida SsoUnauthorizedException.
	 * @return errorResponse Objeto de respuesta específica para
	 *         SsoUnauthorizedException.
	 */
	@ExceptionHandler(SsoUnauthorizedException.class)
	@ResponseStatus(value = HttpStatus.FORBIDDEN)
	public ErrorResponse resolveSsoUnauthorizedException(HttpServletRequest req, SsoUnauthorizedException ex) {
		log.debug(errorResolverConstants.getMensajeSsoTokenInvalido());

		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setType(ErrorType.INVALID.name());
		errorResponse.setDetails(errorResolverConstants.getMensajeDefaultError());
		errorResponse.setTimestamp(ZonedDateTime.now());
		errorResponse.setUuid(req.getHeader(ApiConstant.UUID));
		errorResponse.setCode(errorResolverConstants.getCodigoSsoTokenInvalido());
		errorResponse.setLocation(req.getRequestURI());

		ErrorResolver.writeToLog(errorResponse, ex);

		return errorResponse;
	}

//	/**
//	 * Manejador para la excepcion CryptographiSecurityException.
//	 * 
//	 * @param req Objeto HttpServletRequest.
//	 * @param ex  Excepcion lanzada CryptographiSecurityException.
//	 * @return ErrorResponse error de retorno.
//	 */
//	@ExceptionHandler(CryptographiSecurityException.class)
//	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
//	public ErrorResponse resolveCryptographiSecurityException(HttpServletRequest req,
//			CryptographiSecurityException ex) {
//		ErrorResponse errorResponse = new ErrorResponse();
//		log.debug(ex.getMessage());
//		errorResponse.setType(ErrorType.ERROR.name());
//		errorResponse.setCode(errorResolverConstants.getBadRequestException());
//		errorResponse.setDetails(errorResolverConstants.getMensajeDefaultError());
//		errorResponse.setTimestamp(ZonedDateTime.now());
//		errorResponse.setUuid(req.getHeader(ApiConstant.UUID));
//		errorResponse.setLocation(req.getRequestURI());
//
//		ErrorResolver.writeToLog(errorResponse, ex);
//
//		return errorResponse;
//	}

	/**
	 * Maneja la excepcion ReverseNotValidException.
	 * 
	 * @param req Objeto Http Servlet de petición.
	 * @param ex  Excepción recibida ReverseNotValidException.
	 * @return errorResponse Objeto de respuesta específica para
	 *         ReverseNotValidException.
	 */
	@ExceptionHandler(ReverseNotValidException.class)
	@ResponseStatus(value = HttpStatus.FORBIDDEN)
	public ErrorResponse resolveCardNotValidException(HttpServletRequest req, ReverseNotValidException ex) {

		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setType(ErrorType.INVALID.name());
		errorResponse.setDetails(errorResolverConstants.getMensajeReverseInvalida());
		errorResponse.setTimestamp(ZonedDateTime.now());
		errorResponse.setUuid(req.getHeader(ApiConstant.UUID));
		errorResponse.setCode(errorResolverConstants.getCodigoReverseInvalida());
		errorResponse.setLocation(req.getRequestURI());

		ErrorResolver.writeToLog(errorResponse, ex);

		return errorResponse;
	}

	/**
	 * Maneja la excepción RetryableException.
	 * 
	 * @param req request
	 * @param ex  excepcion
	 * @return retorna un objeto tipo ErrorResponse
	 */
	@ExceptionHandler(RetryableException.class)
	@ResponseStatus(value = HttpStatus.REQUEST_TIMEOUT)
	public ErrorResponse resolveTimeOutRetryableException(HttpServletRequest req, RetryableException ex) {
		ErrorResponse errorResponse = new ErrorResponse();

		errorResponse.setType(ErrorType.ERROR.name());
		errorResponse.setCode(errorResolverConstants.getTimeOutCodeException());
		errorResponse.setLocation(req.getRequestURI());
		errorResponse.setDetails(errorResolverConstants.getTimeOutException());
		errorResponse.setTimestamp(ZonedDateTime.now());
		errorResponse.setUuid(req.getHeader(ApiConstant.UUID));

		ErrorResolver.writeToLog(errorResponse, ex);

		return errorResponse;
	}

	/**
	 * Maneja la excepción TimeoutException.
	 * 
	 * @param req request
	 * @param ex  excepcion
	 * @return retorna un objeto tipo ErrorResponse
	 */
	@ExceptionHandler(TimeoutException.class)
	@ResponseStatus(value = HttpStatus.REQUEST_TIMEOUT)
	public ErrorResponse resolveTimeoutException(HttpServletRequest req, TimeoutException ex) {
		ErrorResponse errorResponse = new ErrorResponse();

		errorResponse.setType(ErrorType.ERROR.name());
		errorResponse.setCode(errorResolverConstants.getTimeOutCodeException());
		errorResponse.setLocation(req.getRequestURI());
		errorResponse.setDetails(errorResolverConstants.getTimeOutException());
		errorResponse.setTimestamp(ZonedDateTime.now());
		errorResponse.setUuid(req.getHeader(ApiConstant.UUID));

		ErrorResolver.writeToLog(errorResponse, ex);

		return errorResponse;
	}

	/**
	 * Handler for RedisNotValidException.
	 *
	 * @param req of type HttpServletRequest.
	 * @param ex  of type UnauthorizedException.
	 * @return errorResponse object.
	 */
	@ExceptionHandler(RedisNotValidException.class)
	@ResponseStatus(value = HttpStatus.FORBIDDEN)
	public ErrorResponse resolveRedisNotValidException(HttpServletRequest req, RedisNotValidException ex) {
		ErrorResponse errorResponse = new ErrorResponse();

		errorResponse.setType(ErrorType.ERROR.name());
		errorResponse.setCode(ex.getCode());
		errorResponse.setDetails(errorResolverConstants.getMensajeDefaultError());
		errorResponse.setTimestamp(ZonedDateTime.now());
		errorResponse.setUuid(req.getHeader(ApiConstant.UUID));
		errorResponse.setLocation(req.getRequestURI());

		ErrorResolver.writeToLog(errorResponse, ex);

		return errorResponse;
	}

	/**
	 * Metodo para captar la excepcion de Intentos invalidos.
	 * 
	 * @param req datos del HTTP.
	 * @param ex  Excepcion de los headers no validados.
	 * @return ErrorResponse.
	 */
	@ExceptionHandler(InvalidAttemptException.class)
	@ResponseStatus(value = HttpStatus.FORBIDDEN)
	public ErrorResponse invalidAttemptException(HttpServletRequest req, InvalidAttemptException ex) {
		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setType(ErrorType.ERROR.name());
		errorResponse.setCode(errorResolverConstants.getCodigoSsoTokenInvalido());
		errorResponse.setDetails(ex.getMessage());
		errorResponse.setLocation(req.getRequestURI());
		errorResponse.setTimestamp(ZonedDateTime.now());
		errorResponse.setUuid(req.getHeader(ApiConstant.UUID));
		ErrorResolver.writeToLog(errorResponse, ex);

		return errorResponse;
	}

	/**
	 * Metodo para captar la excepcion de reintentos invalidos.
	 * 
	 * @param req datos del HTTP.
	 * @param ex  Excepcion de los headers no validados.
	 * @return ErrorResponse.
	 */
	@ExceptionHandler(InvalidSsoExeption.class)
	@ResponseStatus(value = HttpStatus.FORBIDDEN)
	public ErrorResponse invalidSsoExeption(HttpServletRequest req, InvalidSsoExeption ex) {
		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setType(ErrorType.ERROR.name());
		errorResponse.setCode(errorResolverConstants.getForbiddenException());
		errorResponse.setDetails(ex.getMessage());
		errorResponse.setLocation(req.getRequestURI());
		errorResponse.setTimestamp(ZonedDateTime.now());
		errorResponse.setUuid(req.getHeader(ApiConstant.UUID));
		ErrorResolver.writeToLog(errorResponse, ex);

		return errorResponse;
	}

}
