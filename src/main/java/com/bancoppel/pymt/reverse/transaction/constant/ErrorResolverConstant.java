package com.bancoppel.pymt.reverse.transaction.constant;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Getter;

/**
 * Constants class para dar de alta los parametros de errores.
 */
@Component
@EnableConfigurationProperties
@Getter
public class ErrorResolverConstant {

	/**
	 * Constant to represent the string 'errorResponse.type, {}'.
	 */
	public static final String ERROR_RESPONSE_TYPE_INFO = "type, {}";

	/**
	 * Constant to represent the string 'errorResponse.code, {}'.
	 */
	public static final String ERROR_RESPONSE_CODE_INFO = "code, {}";

	/**
	 * Constant to represent the string 'errorResponse.details, {}'.
	 */
	public static final String ERROR_RESPONSE_DETAILS_INFO = "details, {}";

	/**
	 * Constant to represent the string 'errorResponse.location, {}'.
	 */
	public static final String ERROR_RESPONSE_LOCATION_INFO = "location, {}";

	/**
	 * Constant to represent the string 'errorResponse.moreInfo, {}'.
	 */
	public static final String ERROR_RESPONSE_MORE_INFORMATION_INFO = "moreInfo, {}";

	/**
	 * Constant to represent the string 'code'.
	 */
	public static final String ERROR_RESPONSE_CODE = "code";

	/**
	 * Constant used to represent an error code caused by a BadRequestException.
	 */
	@Value("${constants.errorResolver.errorCodes.dataNotFoundException}")
	private String dataNotFoundException;

	/**
	 * Constant used to represent an error code caused by a NoHandlerFoundException.
	 */
	@Value("${constants.errorResolver.errorCodes.noHandlerFoundException}")
	private String noHandlerFoundException;

	/**
	 * Variable que contiene el mensaje default Algo ha salido mal!!.
	 */
	@Value("${constants.errorResolver.messages.error}")
	private String mensajeDefaultError;

	/**
	 * Constant used to represent an error code caused by a
	 * HttpMediaTypeNotAcceptableException.
	 */
	@Value("${constants.errorResolver.errorCodes.httpMediaTypeNotAcceptableException}")
	private String httpMediaTypeNotAcceptableException;

	/**
	 * Constant used to represent an error code caused by a
	 * HttpRequestMethodNotSupportedException.
	 */
	@Value("${constants.errorResolver.errorCodes.httpRequestMethodNotSupportedException}")
	private String httpRequestMethodNotSupportedException;

	/**
	 * Constant used to represent an error code caused by a
	 * HttpMediaTypeNotSupportedException.
	 */
	@Value("${constants.errorResolver.errorCodes.httpMediaTypeNotSupportedException}")
	private String httpMediaTypeNotSupportedException;

	/**
	 * Constant used to represent an error code caused by a
	 * ServletRequestBindingException.
	 */
	@Value("${constants.errorResolver.errorCodes.servletRequestBindingException}")
	private String servletRequestBindingException;

	/**
	 * Constant that is used to show a message about an unknown error.
	 */
	@Value("${constants.errorResolver.messages.genericErrorDescription}")
	private String genericErrorDescription;

	/**
	 * Constant used to represent an error code caused by a
	 * HttpMessageNotReadableException.
	 */
	@Value("${constants.errorResolver.errorCodes.httpMessageNotReadableException}")
	private String httpMessageNotReadableException;

	/**
	 * Constant used to represent an error code caused by a
	 * MethodArgumentNotValidException.
	 */
	@Value("${constants.errorResolver.errorCodes.methodArgumentNotValidException}")
	private String methodArgumentNotValidException;

	/**
	 * Constant used to represent an error code caused by a
	 * ConstraintViolationException.
	 */
	@Value("${constants.errorResolver.errorCodes.constraintViolationException}")
	private String constraintViolationException;

	/**
	 * Constant used to represent an error code caused by an unknown error.
	 */
	@Value("${constants.errorResolver.errorCodes.exception}")
	private String exception;

	/**
	 * Constant used to represent an error code caused by a HystrixRuntimeException.
	 */
	@Value("${constants.errorResolver.errorCodes.hystrixRuntimeException}")
	private String hystrixRuntimeException;

	/**
	 * Constant used to represent an error code caused by a BadRequestException.
	 */
	@Value("${constants.errorResolver.errorCodes.badRequestException}")
	private String badRequestException;

	/**
	 * Constant used to show the message 'Invalid Request'.
	 */
	@Value("${constants.errorResolver.messages.servicioNoDisponible}")
	private String mensajeServicioNoDisponible;
	
	
	  /**
	   * Constant used to show the message 'Invalid Request'.
	   */
	  @Value("${constants.errorResolver.errorCodes.servicioNoDisponible}")
	  private String codigoServicioNoDisponible;
	
	  /**
	   * Constant used to show the message 'Invalid Request'.
	   */
	  @Value("${constants.errorResolver.messages.errorEnPago}")
	  private String mensajeErrorEnPago;
	
	  /**
	   * Constant used to show the message 'Invalid Request'.
	   */
	  @Value("${constants.errorResolver.errorCodes.errorEnPago}")
	  private String codigoErrorEnPago;
	
	
	  /**
	   * Constant used to show the message 'Invalid Request'.
	   */
	  @Value("${constants.errorResolver.messages.gemaltoTokenInvalido}")
	  private String mensajeGemaltoTokenInvalido;
	  
	  /**
	   * Constant used to show the message 'Invalid Request'.
	   */
	  @Value("${constants.errorResolver.errorCodes.gemaltoTokenInvalido}")
	  private String codigoGemaltoTokenInvalido;
	  

	  /**
	   * Constant used to show the message 'Invalid Request'.
	   */
	  @Value("${constants.errorResolver.messages.ssoTokenInvalido}")
	  private String mensajeSsoTokenInvalido;
	
	
	  /**
	   * Constant used to show the message 'Invalid Request'.
	   */
	  @Value("${constants.errorResolver.errorCodes.ssoTokenInvalido}")
	  private String codigoSsoTokenInvalido;
	
	  /**
	   * Constant used to show the message 'Invalid Request'.
	   */
	  @Value("${constants.errorResolver.messages.reverse}")
	  private String mensajeReverseInvalida;
	  
	  /**
	   * Constant used to show the message 'Invalid Request'.
	   */
	  @Value("${constants.errorResolver.errorCodes.reverseInvalido}")
	  private String codigoReverseInvalida;
	  
	  /**
	   * Constant used to represent an error code caused by a timeOutCodeException.
	   */
	  @Value("${constants.errorResolver.errorCodes.requestTimeoutException}")
	  private String timeOutCodeException;
	  
	  /**
	   * Constante para mostrar un mensaje para la excepción TimeOutException.
	   */
	  @Value("${constants.errorResolver.messages.timeOutException}")
	  private String timeOutException;
	
	  /**
	   * Constant used to show the message 'Invalid Request'.
	   */
	  @Value("${constants.errorResolver.errorCodes.forbiddenException}")
	  private String forbiddenException;
	
	

}
