package com.bancoppel.pymt.reverse.transaction.constant;

/**
 * Clase que contiene los valores de las constantes para la documentacion de las
 * API's.
 *
 * @author dmr
 */
public class ConstantSwagger {

	/**
	 * Constante para la palabra header.
	 */
	public static final String HEADER = "header";

	/**
	 * Constante utilizada para representar el nombre del header Accept.
	 */
	public static final String ACCEPT = "Accept";

	/**
	 * Constante con la descripcion del header Accept.
	 */
	public static final String ACCEPT_HEADER_DESCRIPTION = "Media type que son aceptados en la peticion.";

	/**
	 * Constante utilizada para representar el nombre del header Authorization.
	 */
	public static final String AUTHORIZATION = "Authorization";

	/**
	 * Constante utilizada para mostrar la descripcion del authorization.
	 */
	public static final String AUTHORIZATION_HEADER_DESCRIPTION = "Token de autorización recibido durante el login.";

	/**
	 * Constante utilizada para representar el nombre del header Content-Type.
	 */
	public static final String CONTENT_TYPE = "Content-Type";

	/**
	 * Constante con la descripcion del header Content-Type.
	 */
	public static final String CONTENT_TYPE_HEADER_DESCRIPTION = "Media type que son enviados en la peticion.";

	/**
	 * Constante utilizada para representar el nombre del header uuid.
	 */
	public static final String UUID = "uuid";

	/**
	 * Constante con la descripcion del header uuid.
	 */
	public static final String UUID_HEADER_DESCRIPTION = "UUID que se genera para cada sesion.";

	/**
	 * Constante utilizada para representar el nombre del header Date.
	 */
	public static final String DATE = "Date";

	/**
	 * Constante con la descripcion del header Date.
	 */
	public static final String DATE_HEADER_DESCRIPTION = "Fecha del dispositivo o del servidor de acuerdo a HTTP-Date.";

	/**
	 * Constante utilizada para representar el nombre del header Accept-Charset.
	 */
	public static final String ACCEPT_CHARSET = "Accept-Charset";

	/**
	 * Constante con la descripcion del header Accept-Charset.
	 */
	public static final String ACCEPT_CHARSET_HEADER_DESCRIPTION = "Tipo de caracteres de la aplicacion.";

	/**
	 * Constante utilizada para representar el nombre del header Accept-Encoding.
	 */
	public static final String ACCEPT_ENCODING = "Accept-Encoding";

	/**
	 * Constante con la descripcion del header Accept-Encoding.
	 */
	public static final String ACCEPT_ENCODING_HEADER_DESCRIPTION = "La aplicacion debera aceptar compresion de datos.";

	/**
	 * Constante utilizada para representar el nombre del header Accept-Language.
	 */
	public static final String ACCEPT_LANGUAGE = "Accept-Language";

	/**
	 * Constante con la descripcion del header Accept-Language.
	 */
	public static final String ACCEPT_LANGUAGE_HEADER_DESCRIPTION = "La aplicacion por defecto usuara español de Mexico requerido.";

	/**
	 * Constante utilizada para representar el nombre del header Host.
	 */
	public static final String HOST = "Host";

	/**
	 * Constante con la descripcion del header Host.
	 */
	public static final String HOST_HEADER_DESCRIPTION = "Host de la aplicacion.";

	/**
	 * Constante utilizada para representar el nombre del header User-Agent.
	 */
	public static final String USER_AGENT = "User-Agent";

	/**
	 * Constante con la descripcion del header User-Agent.
	 */
	public static final String USER_AGENT_HEADER_DESCRIPTION = "Identificador de la version de frontend.";

	/**
	 * Constante utilizada para representar el nombre del header ChannelId.
	 */
	public static final String CHANNEL_ID = "ChannelId";

	/**
	 * Constante utilizada para mostrar la descripcion del channel_id.
	 */
	public static final String CHANNEL_ID_DESCRIPTION = "Canal de la aplicación.";

	/**
	 * Constante utilizada para representar el nombre del header Content-Encoding.
	 */
	public static final String CONTENT_ENCODING = "Content-Encoding";

	/**
	 * Constante con la descripcion del header Content-Encoding.
	 */
	public static final String CONTENT_ENCODING_HEADER_DESCRIPTION = "Datos comprimidos.";

	/**
	 * Constante utilizada para representar el nombre del header Content-Language.
	 */
	public static final String CONTENT_LANGUAGE = "Content-Language";

	/**
	 * Constante con la descripcion del header Content-Language.
	 */
	public static final String CONTENT_LANGUAGE_HEADER_DESCRIPTION = "Lenguaje de la aplicacion.";
	/**
	 * Constante utilizada para representar el nombre del header Content-Length.
	 */
	public static final String CONTENT_LENGTH = "Content-Length";

	/**
	 * Constante con la descripcion del header Content-Length.
	 */
	public static final String CONTENT_LENGTH_HEADER_DESCRIPTION = "Tamaño del mensaje.";

	/**
	 * Constante utilizada para representar el nombre del header Content-MD5.
	 */
	public static final String CONTENT_MD5 = "Content-MD5";

	/**
	 * Constante con la descripcion del header Content-MD5.
	 */
	public static final String CONTENT_MD5_HEADER_DESCRIPTION = "Huella MD5 del contenido del mensaje.";

	/**
	 * Constante utilizada para representar el nombre del header deviceId.
	 */
	public static final String DEVICE_ID = "deviceId";

	/**
	 * Constante utilizada para mostrar la descripcion del device_id.
	 */
	public static final String DEVICE_ID_DESCRIPTION = "Identificador del dispositivo.";

	/**
	 * Constante utilizada para mostrar el tipo de dato "string".
	 */
	public static final String STRING_TYPE = "string";
	
	  /**
	   * Constante que muestra el código ok.
	   */
	  public static final int CODE_OK = 200;
	  
	  /**
	   * Constante utilizada para mostrar la descripcion del status "200".
	   */
	  public static final String OK_DESCRIPTION = "Operacion satisfactoria.";
	  
	  /**
	   * Constante utilizada para mostrar un mensaje acerca de un OK.
	   */
	  public static final String OK = "OK";
	  
	  /**
	   * Constante que muestra el código bad request.
	   */
	  public static final int CODE_BAD_REQUEST = 400;
	  
	  /**
	   * Constante utilizada para mostrar la descripcion del status "400".
	   */
	  public static final String BAD_REQUEST = "Parametros faltantes/Paramteros invalidos.";
	  
	  /**
	   * Constante utilizada para mostrar el status code "404".
	   */
	  public static final int CODE_RESOURCE_NOT_FOUND = 404;
	  
	  /**
	   * Constante utilizada para mostrar la descripcion del status "404".
	   */
	  public static final String RESOURCE_NOT_FOUND = "El recurso solicitado no ha sido encontrado.";
	  
	  /**
	   * Constante utilizada para mostrar el status code 401.
	   */
	  public static final int CODE_UNAUTHORIZED = 401;
	  
	  /**
	   * Constante utilizada para mostrar un mensaje acerca de una petición no autorizada.
	   */
	  public static final String UNAUTHORIZED = "Unauthorized";
	  
	  /**
	   * Constante que muestra el código cod internal error.
	   */
	  public static final int CODE_INTERNAL_ERROR = 500;
	  
	  /**
	   * Constante utilizada para mostrar la descripcion del status "500".
	   */
	  public static final String INTERNAL_ERROR =
	      "La peticion falló debido a un error interno del servidor/Servidor no disponible.";
	  
	  /**
	   * Constante utilizada para mostrar la descripcion del modelo request.
	   */
	  public static final String ORIGIN_ACCOUNT_NUMBER = "Número de Cuenta Origen";
	  
	  
	  /**
	   * Constante utilizada para mostrar la descripcion del modelo request.
	   */
	  public static final String ORIGIN_CUSTOMER_NUMBER = "Número de Cliente Origen";
	  
	  /**
	   * Constante utilizada para mostrar la descripcion del modelo request.
	   */
	  public static final String DESTINATION_ACCOUNT_NUMBER = "Número de Cuenta Destino";
	  
	  /**
	   * Constante utilizada para mostrar la descripcion del modelo request.
	   */
	  public static final String AMOUNT = "Cantidad";
	  
	  /**
	   * Constante utilizada para mostrar la descripcion del modelo request.
	   */
	  public static final String CONCEPT = "Concepto";
	  
	  /**
	   * Constante utilizada para mostrar la descripcion del modelo request.
	   */
	  public static final String TOTAL_AMOUNT = "Cantidad Total";
	  
	  /**
	   * Constante utilizada para mostrar la descripcion del modelo request.
	   */
	  public static final String FIRM_AMOUNT = "Cantidad en Firme";
	  
	  /**
	   * Constante utilizada para mostrar la descripcion del modelo request.
	   */
	  public static final String VIRTUAL_BRANCH = "Sucursal Virtual";
	  
	  
	  /**
	   * Constante utilizada para mostrar la descripcion del modelo request.
	   */
	  public static final String VIRTUAL_USER = "Usuario Virtual";
	  
	  /**
	   * Constante utilizada para mostrar la descripcion del modelo request.
	   */
	  public static final String CARGO_TRANSACCION = "Código de cargoTransaction";
	  
	  /**
	   * Constante utilizada para mostrar la descripcion del modelo request.
	   */
	  public static final String CREDIT_TRANSACCION = "Código de creditTransaction";
	  
	  /**
	   * Constante utilizada para mostrar la descripcion del modelo request.
	   */
	  public static final String BUSINESS = "Código de business";
	  
	  
	  /**
	   * Constante utilizada para mostrar la descripcion del modelo request.
	   */
	  public static final String BRANCH_TRANSACCTION = "Número de transacción de sucursal";
	  
	  /**
	   * Constante utilizada para mostrar la descripcion del modelo request.
	   */
	  public static final String CHECK = "Código de Cheque";
	  
	  /**
	   * Constante utilizada para mostrar la descripcion del modelo request.
	   */
	  public static final String COIN = "Código de Moneda";
	  
	  /**
	   * Constante utilizada para mostrar la descripcion del modelo request.
	   */
	  public static final String USER_AUTHORIZES = "Código de usuario que autoriza";
	  
	  /**
	   * Constante utilizada para mostrar la descripcion del modelo request.
	   */
	  public static final String SBC_AMOUNT = "Cantidad SBC";
	  
	  /**
	   * Constante utilizada para mostrar la descripcion del modelo request.
	   */
	  public static final String REM_AMOUNT = "Cantidad RESTANTE";
	  
	  
	  /**
	   * Constante utilizada para mostrar la descripcion del modelo request.
	   */
	  public static final String RET_DAYS = "Código retDays";
	  
	  /**
	   * Constante utilizada para mostrar la descripcion del modelo request.
	   */
	  public static final String DOCTO = "Documento";
	  
	  /**
	   * Constante utilizada para mostrar la descripcion del modelo request.
	   */
	  public static final String TYPE_REVERSION = "Código de typeReversion";
	  
	  /**
	   * Constante utilizada para mostrar la descripcion del modelo request.
	   */
	  public static final String ID_SERVICE_PAYMENT = "Id del servicio a pagar.";

	  
	  
	  
	  
	  
	  
}
