/**
 * 
 */
package com.bancoppel.pymt.reverse.transaction.service;

import org.springframework.http.HttpHeaders;

import com.bancoppel.pymt.reverse.transaction.model.RequestReverse;
import com.bancoppel.pymt.reverse.transaction.model.ResponseReverse;



/**
 * Service interface. * @author dmr
 */
public interface IReverseTransaction {
	
	/**
	   * Metodo que consulta microservicio  msadp-d-domain-apply-intrabank-transfer para ejecutar el reverso
	   * 
	   * @param RequestReverse reques a api.
	   * @param headers de peticion.
	   * @param localDate objeto fecha.
	   * @param localTime objeto hora.
	   * @param localDateTime objeto fecha y hora.
	   * @return response de api.
	   */
	  ResponseReverse reverseTransactionService(RequestReverse requestReverse,
	      HttpHeaders headers);

}
