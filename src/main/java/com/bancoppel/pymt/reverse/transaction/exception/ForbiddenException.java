package com.bancoppel.pymt.reverse.transaction.exception;

import com.bancoppel.pymt.reverse.transaction.constant.ApiConstant;

import lombok.Getter;

@Getter
public class ForbiddenException extends RuntimeException{

	private final String code;

	  /**
	   * UID default de la clase.
	   */
	  private static final long serialVersionUID = 1L;

	  /**
	   * Constructor por default.
	   */
	  public ForbiddenException() {
	    super(ApiConstant.EMPTY_STRING);
	    this.code = "";
	  }
}
