package com.bancoppel.pymt.reverse.transaction.exception;

import lombok.Getter;

@Getter
public class RedisNotValidException extends RuntimeException {

  /**
   * Generated serial unique ID.
   */
  private static final long serialVersionUID = -7894187026358558149L;

  private final String code;

  /**
   * Crea una excepcion con su causa y mensaje de error.
   * 
   * @param message Mensaje detallando el error.
   * @param code codigo del error.
   */
  public RedisNotValidException(String message, String code) {
    super(message);
    this.code = code;
  }

  /**
   * Constructor that receives the error message.
   * 
   * @param code code de exception.
   */
  public RedisNotValidException(String code) {
    this.code = code;
  }
}