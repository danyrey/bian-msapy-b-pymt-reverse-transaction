package com.bancoppel.pymt.reverse.transaction.business;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;

import com.bancoppel.commons.annotation.HandledProcedure;

import com.bancoppel.it.management.sso.exception.InvalidAttemptException;
import com.bancoppel.it.management.sso.exception.InvalidSsoExeption;
import com.bancoppel.it.management.sso.service.SsoCustomerService;
import com.bancoppel.ptsv.log.domain.TransactionalLog;
import com.bancoppel.pymt.reverse.transaction.constant.ApiConstant;
import com.bancoppel.pymt.reverse.transaction.constant.ApiValues;
import com.bancoppel.pymt.reverse.transaction.constant.LogValues;
import com.bancoppel.pymt.reverse.transaction.exception.DownstreamException;
import com.bancoppel.pymt.reverse.transaction.exception.InvalidGemaltoTokenException;
import com.bancoppel.pymt.reverse.transaction.exception.ReverseNotValidException;
import com.bancoppel.pymt.reverse.transaction.model.ApplyThirdPartyTransferRequest;
import com.bancoppel.pymt.reverse.transaction.model.ApplyThirdPartyTransferResponse;
import com.bancoppel.pymt.reverse.transaction.model.Bitacora;
import com.bancoppel.pymt.reverse.transaction.model.GemaltoTokenRequest;
import com.bancoppel.pymt.reverse.transaction.model.RequestReverse;
import com.bancoppel.pymt.reverse.transaction.model.ResponseReverse;
import com.bancoppel.pymt.reverse.transaction.service.IGemaltoTokenFeign;
import com.bancoppel.pymt.reverse.transaction.service.IReverseTransaction;
import com.bancoppel.pymt.reverse.transaction.service.IintrabankTransferFeign;
import com.bancoppel.pymt.reverse.transaction.util.Util;

import feign.RetryableException;
import lombok.extern.slf4j.Slf4j;

/**
 * Clase que implementa la interfaz IReverseTransaction y que contiene la logica
 * de negocio para el llamado del microservicio
 * msadp-d-domain-apply-intrabank-transfer.
 *
 * @author dmr
 */
@Service
@Slf4j
public class ReverseTransactionBusiness implements IReverseTransaction {

	/**
	 * Clase donde estan los valores de la aplicacion.
	 */
	@Autowired
	ApiValues apivalues;

	/**
	 * Clase donde estan los valores del log para la bitacora.
	 */
	@Autowired
	LogValues logValues;

	/**
	 * Interfaz de dependencia de bitácora.
	 */
	@Autowired
	private BitacoraBusiness bitacoraB;

	/**
	 * Clase para poder sacar el custumer del sso.
	 */
	@Autowired
	private SsoCustomerService ssoCustomerService;

	/**
	 * Feign para validar token de gemalto.
	 */
	@Autowired
	private IGemaltoTokenFeign gemaltoTokenFeign;

	/**
	 * Feign para hacer el reverso del pago de servicio.
	 */
	@Autowired
	private IintrabankTransferFeign intrabankTransferFeign;

	/**
	 * Metodo que ejecuta logica de negocio para realizar el reverso desde el
	 * microservicio de envío intrabancario.
	 * 
	 * @param RequestReverse que se enviara al feinclient .
	 * @param headers        que se enviara al feinclient .
	 * @return ResponseReverse .
	 */
	@HandledProcedure(name = ApiConstant.BUSINESS_SERVICE_LOG, value = ApiConstant.BUSINESS_SERVICE_LOG, ignoreExceptions = {
			ReverseNotValidException.class, InvalidGemaltoTokenException.class, RetryableException.class,
			DownstreamException.class, InvalidAttemptException.class, InvalidSsoExeption.class })
	@Override
	public ResponseReverse reverseTransactionService(RequestReverse requestReverse, HttpHeaders headers) {
		log.info("**inicia proceso de reverso", headers.getFirst("Authorization"));

		// valida si numero de cuenta es diferente a 11 no pase
		log.info("**Valida numero de cuenta a 11 posiciones  :  " + requestReverse.getOriginAccountNumber());
		if (requestReverse.getOriginAccountNumber().length() != apivalues.getValidLongAccount()) {
			throw new ReverseNotValidException();
		}

		// Ponemos en el log el custumernumber
		log.info("**Pide custumer al SSO");
		// String customerNumber =
		// ssoCustomerService.getCustomerNumber(headers.getFirst(ApiConstant.AUTHORIZATION));
		String customerNumber = "juan";
		log.debug(ApiConstant.CUSTOMER_LOG, customerNumber);

		// Guarda en la bitacora headers, requestReverse, custumerNumber , id servicio a
		// pagar.
		log.info("**Inicia llenado de bitacora", customerNumber);
		Bitacora bitacora = initializeValuesBitacora(headers, requestReverse, customerNumber,
				requestReverse.getIdServicePayment());

		// Valida el gemalto token
		// validationGemaltoTokenFeign(headers, customerNumber,
		// requestReverse.getOtp());
		
		log.info("**Consulta fign de intrabank", customerNumber);
//		ApplyThirdPartyTransferResponse applyThirdPartyTransferResponse = intrabankTransferFeign.executeTransfer(
//				headers.getFirst(ApiConstant.ACCEPT), headers.getFirst(ApiConstant.AUTHORIZATION),
//				headers.getFirst(ApiConstant.UUID), headers.getFirst(ApiConstant.DATE),
//				headers.getFirst(ApiConstant.ACCEPT_CHARSET), headers.getFirst(ApiConstant.ACCEPT_ENCODING),
//				headers.getFirst(ApiConstant.ACCEPT_LANGUAGE), headers.getFirst(ApiConstant.HOST),
//				headers.getFirst(ApiConstant.USER_AGENT), headers.getFirst(ApiConstant.CHANNEL_ID),
//				headers.getFirst(ApiConstant.CONTENT_ENCODING), headers.getFirst(ApiConstant.CONTENT_LANGUAGE),
//				headers.getFirst(ApiConstant.CONTENT_MD5), headers.getFirst(ApiConstant.DEVICE_ID),
//				tranformaReverseRequest(requestReverse));

		log.info("** Guarda bitacora en mongo", headers.getFirst("Authorization"));
		 bitacoraB.saveBitacora(bitacora);
		log.info("**Termina proceso de reverso", headers.getFirst("Authorization"));
		// return tranformaReverseResponse(applyThirdPartyTransferResponse);
		ResponseReverse reverse = new ResponseReverse();
		reverse.setInvoiceBranch("12345");
		return reverse;
	}

	/**
	 * Metodo que asigna valores iniciales para guardar en bitacora.
	 * 
	 * @param headers             headers.
	 * @param speiTransferRequest valores de la petición
	 * @param customerNumber      numero de cliente
	 * @param idOperation         id de operacion para bitacora de acuerdo a
	 *                            servicio
	 * @return valores de bitacora
	 */
	private Bitacora initializeValuesBitacora(MultiValueMap<String, String> headers, RequestReverse requestReverse,
			String customerNumber, String idOperation) {

		Bitacora bitacora = new Bitacora();
		bitacora.setOperationId(idOperation);
		bitacora.setCustomerNumber(customerNumber);
		bitacora.setDeviceId(headers.getFirst(ApiConstant.DEVICE_ID));
		bitacora.setUuid(headers.getFirst(ApiConstant.UUID));
		bitacora.setLogType(logValues.getLogTypeTransaccional());
		bitacora.setHttpStatus(HttpStatus.OK.value());
		bitacora.setCode(LogValues.SUCCESS_CODE);

		TransactionalLog transactionLog = new TransactionalLog();
		transactionLog.setAmountOperation(String.valueOf(requestReverse.getAmount()));
		transactionLog.setTargetAccount(requestReverse.getOriginAccountNumber());
		bitacora.setTransactionLog(transactionLog);

		return bitacora;
	}

	/**
	 * Metodo que consume la Api GemaltoToken para validar si el OTP es valido en
	 * Gemalto.
	 * 
	 * @param headers        Cabeceras de la petición.
	 * @param customerNumber Numero de Usuario.
	 * @param otp            One Time Password de Gemalto.
	 * @return boolean result.
	 */
	private boolean validationGemaltoTokenFeign(MultiValueMap<String, String> headers, String customerNumber,
			String otp) {
		GemaltoTokenRequest gemaltoTokenRequest = new GemaltoTokenRequest();
		gemaltoTokenRequest.setCustomerNumber(customerNumber);
		gemaltoTokenRequest.setOtp(otp);
		gemaltoTokenFeign.validateGemaltoToken(headers.getFirst(ApiConstant.ACCEPT),
				headers.getFirst(ApiConstant.AUTHORIZATION), headers.getFirst(ApiConstant.APPLICATION_NAME),
				headers.getFirst(ApiConstant.DEVICE_ID), headers.getFirst(ApiConstant.UUID), gemaltoTokenRequest);
		return true;
	}

	private ApplyThirdPartyTransferRequest tranformaReverseRequest(RequestReverse requestReverse) {
		ApplyThirdPartyTransferRequest applyRequest = new ApplyThirdPartyTransferRequest();
		applyRequest.setOriginAccountNumber(requestReverse.getOriginAccountNumber());
		applyRequest.setOriginCustomerNumber(requestReverse.getOriginCustomerNumber());
		applyRequest.setDestinationAccountNumber(requestReverse.getDestinationAccountNumber());
		applyRequest.setAmount(requestReverse.getAmount());
		applyRequest.setConcept(requestReverse.getConcept());
		applyRequest.setTotalAmount(requestReverse.getTotalAmount());
		applyRequest.setFirmAmount(requestReverse.getFirmAmount());
		applyRequest.setVirtualBranch(requestReverse.getVirtualBranch());
		applyRequest.setVirtualUser(requestReverse.getVirtualUser());
		applyRequest.setCargoTransaction(requestReverse.getOriginCustomerNumber());
		applyRequest.setCreditTransaction(requestReverse.getCreditTransaction());
		applyRequest.setBusiness(requestReverse.getBusiness());
		applyRequest.setBranchTransaction(requestReverse.getBranchTransaction());
		applyRequest.setCheck(requestReverse.getCheck());
		applyRequest.setCoin(requestReverse.getCoin());
		applyRequest.setUserAuthorizes(requestReverse.getUserAuthorizes());
		applyRequest.setSbcAmount(requestReverse.getSbcAmount());
		applyRequest.setRemAmount(requestReverse.getRemAmount());
		applyRequest.setRetDays(requestReverse.getRetDays());
		applyRequest.setDocto(requestReverse.getDocto());
		applyRequest.setTypeReversion(requestReverse.getTypeReversion());

		return applyRequest;

	}

	private ResponseReverse tranformaReverseResponse(ApplyThirdPartyTransferResponse applyTransferResponse) {
		ResponseReverse responseReverse = new ResponseReverse();
		responseReverse.setInvoiceBranch(applyTransferResponse.getInvoiceBranch());
		return responseReverse;

	}

}
