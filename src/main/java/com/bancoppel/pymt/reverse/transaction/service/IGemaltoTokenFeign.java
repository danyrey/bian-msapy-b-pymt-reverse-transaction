package com.bancoppel.pymt.reverse.transaction.service;

import java.util.concurrent.CompletionException;
import java.util.concurrent.TimeoutException;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

import com.bancoppel.commons.annotation.HandledProcedure;
import com.bancoppel.commons.exceptions.ExternalResourceException;
import com.bancoppel.commons.exceptions.NotValidHeadersException;
import com.bancoppel.pymt.reverse.transaction.constant.ApiConstant;
import com.bancoppel.pymt.reverse.transaction.constant.FeignConstants;
import com.bancoppel.pymt.reverse.transaction.exception.DownstreamException;
import com.bancoppel.pymt.reverse.transaction.exception.InvalidGemaltoTokenException;
import com.bancoppel.pymt.reverse.transaction.model.GemaltoTokenRequest;

import feign.FeignException;
import feign.RetryableException;


/**
 * Interfaz de tipo Feign que consume el Api GetFlagSpei.
 * 
 * @author Pedro Reyes
 */
@FeignClient(url = FeignConstants.GEMALTO_TOKEN_URL, name = FeignConstants.GEMALTO_TOKEN_NAME)
public interface IGemaltoTokenFeign {
	
	  /**
	   * Metodo que consume al Api GetFlagSpei.
	   * 
	   * @param accept Cabecera Accept.
	   * @param authorization Cabecera Authorization.
	   * @param channelId Cabecera channel_id.
	   * @param deviceId Cabecera deviceId.
	   * @param uuid Cabecera uuid.
	   * @param gemaltoTokenRequest Objeto de peticion para gemaltoToken.
	   * @return Respuesta HttpStatus.
	   */
	  @HandledProcedure(name = ApiConstant.LOG_GEMALTO_TOKEN, value = ApiConstant.LOG_GEMALTO_TOKEN,
	      ignoreExceptions = {DownstreamException.class, FeignException.class,
	          NotValidHeadersException.class, CompletionException.class,
	          ExternalResourceException.class, InvalidGemaltoTokenException.class,
	          RetryableException.class, TimeoutException.class})
	  @PostMapping(value = FeignConstants.GEMALTO_TOKEN_ENDPOINT)
	  ResponseEntity<HttpStatus> validateGemaltoToken(
	      @RequestHeader(name = ApiConstant.ACCEPT) String accept,
	      @RequestHeader(name = ApiConstant.AUTHORIZATION) String authorization,
	      @RequestHeader(name = ApiConstant.CHANNEL_ID) String channelId,
	      @RequestHeader(name = ApiConstant.DEVICE_ID) String deviceId,
	      @RequestHeader(name = ApiConstant.UUID) String uuid,
	      @RequestBody GemaltoTokenRequest gemaltoTokenRequest);

}
