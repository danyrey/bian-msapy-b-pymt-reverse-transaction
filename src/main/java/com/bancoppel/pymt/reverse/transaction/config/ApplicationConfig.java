package com.bancoppel.pymt.reverse.transaction.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.validation.beanvalidation.MethodValidationPostProcessor;

import com.bancoppel.pymt.reverse.transaction.component.CustomFeignErrorDecoder;

/**
 * Configuration class that defines configuration beans needed for the microservice.
 */
@Configuration
public class ApplicationConfig {

  /**
   * Required bean for validating path variables, request params and query params.
   * 
   * @return bean of MethodValidationPostProcessor.
   */
  @Bean
  public MethodValidationPostProcessor methodValidationPostProcessor() {
    return new MethodValidationPostProcessor();
  }

  /**
   * Required bean to allow feign client process responses diferent from http status code 200.
   * 
   * @return bean of CustomFeignErrorDecoder.
   */
  @Bean
  @Primary
  public CustomFeignErrorDecoder customFeignErrorDecoder() {
    return new CustomFeignErrorDecoder();
  }
}
