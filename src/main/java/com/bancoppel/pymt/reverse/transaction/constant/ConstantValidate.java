package com.bancoppel.pymt.reverse.transaction.constant;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * Clase de constantes para validaciones de modelos.
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ConstantValidate {

	/**
	 * Constante que define el mensaje de error cuando un valor esta nulo o vacio.
	 */
	public static final String MSG_NOT_VALUE = "El valor no puede ser nulo: ";

	/**
	 * Constant used to represent an empty string.
	 */
	public static final String EMPTY_STRING = "";

	/**
	 * Constant que representa el string 'type'.
	 */
	public static final String ERROR_RESPONSE_TYPE = "type";
	
	  /**
	   * Constant que representa el string 'code'.
	   */
	  public static final String ERROR_RESPONSE_CODE = "code";
	  
	  /**
	   * Constant to represent the string 'location'.
	   */
	  public static final String ERROR_RESPONSE_LOCATION = "location";
	  
	  /**
	   * Constant to represent the string 'moreInfo'.
	   */
	  public static final String ERROR_RESPONSE_MORE_INFORMATION = "moreInfo";
	  
	  /**
	   * Constant that represents number zero Integer.
	   */
	  public static final int INT_ZERO_VALUE = 0;
	  
	  /**
	   * Constant that represents number two Integer.
	   */
	  public static final int INT_TWO_VALUE = 2;

}
