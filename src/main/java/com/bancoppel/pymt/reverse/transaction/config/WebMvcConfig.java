package com.bancoppel.pymt.reverse.transaction.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.bancoppel.pymt.reverse.transaction.constant.ApiValues;



/**
 * Configuration class to modifify web-mvc context.
 */
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

  /**
   * Injected bean to retrieve the constants defined in external yaml file.
   */
  @Autowired
  private ApiValues apiValues;

  /**
   * Overrides the auto-configuration to ignore the default response for the accept header.
   *
   * @param configurer - configurer
   */
  @Override
  public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
    configurer.favorPathExtension(Boolean.TRUE).ignoreAcceptHeader(Boolean.TRUE);
  }

  /**
   * Overrides to indicate the path of the resource handlers.
   */
  @Override
  public void addResourceHandlers(ResourceHandlerRegistry registry) {
    registry.addResourceHandler(apiValues.getLabel())
        .addResourceLocations(apiValues.getResourceLocation());
    registry.addResourceHandler(apiValues.getWebjars())
        .addResourceLocations(apiValues.getWebjarsLocation());

    WebMvcConfigurer.super.addResourceHandlers(registry);
  }
}
