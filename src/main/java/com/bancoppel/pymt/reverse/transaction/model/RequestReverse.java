/***********************************************************************
 * Copyright (c) 2023 Kairosds Solutions S.A. de C.V. All rights reserved.
 *
 * Licensed under the GNU General Public License, Version 3 (the 
 * "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at
 *
 *   https://www.gnu.org/licenses/gpl-3.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or 
 * implied.
 * 
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **********************************************************************/
package com.bancoppel.pymt.reverse.transaction.model;

import java.math.BigDecimal;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.bancoppel.pymt.reverse.transaction.constant.ConstantValidate;
import com.bancoppel.pymt.reverse.transaction.constant.ApiValues;
import com.bancoppel.pymt.reverse.transaction.constant.ConstantSwagger;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * Request para generar el reverso de un pago
 * 
 * @author dmr
 *
 */
@Data
@ApiModel("RequestReverse")
public class RequestReverse {

	/**
	 * Número de Cuenta Origen.
	 */
	@ApiModelProperty(value = ConstantSwagger.ORIGIN_ACCOUNT_NUMBER, required = true)
	@NotNull(message = ConstantValidate.MSG_NOT_VALUE)
	private String originAccountNumber;

	/**
	 * Número de Cliente Origen.
	 */
	@ApiModelProperty(value = ConstantSwagger.ORIGIN_CUSTOMER_NUMBER, required = true)
	@NotNull(message = ConstantValidate.MSG_NOT_VALUE)
	private String originCustomerNumber;

	/**
	 * Número de Cuenta Destino.
	 */
	@ApiModelProperty(value = ConstantSwagger.DESTINATION_ACCOUNT_NUMBER, required = true)
	@NotNull(message = ConstantValidate.MSG_NOT_VALUE)
	private String destinationAccountNumber;

	/**
	 * Cantidad.
	 */
	@ApiModelProperty(value = ConstantSwagger.AMOUNT, required = true)
	@NotNull(message = ConstantValidate.MSG_NOT_VALUE)
	private BigDecimal amount;

	/**
	 * Concepto.
	 */
	@ApiModelProperty(value = ConstantSwagger.CONCEPT, required = true)
	@NotNull(message = ConstantValidate.MSG_NOT_VALUE)
	private String concept;

	/**
	 * Cantidad Total.
	 */
	@ApiModelProperty(value = ConstantSwagger.TOTAL_AMOUNT, required = true)
	@NotNull(message = ConstantValidate.MSG_NOT_VALUE)
	private BigDecimal totalAmount;

	/**
	 * Cantidad en Firme.
	 */
	@ApiModelProperty(value = ConstantSwagger.FIRM_AMOUNT, required = true)
	@NotNull(message = ConstantValidate.MSG_NOT_VALUE)
	private BigDecimal firmAmount;

	/**
	 * Código de virtualBranch.
	 */
	@ApiModelProperty(value = ConstantSwagger.VIRTUAL_BRANCH, required = true)
	@NotNull(message = ConstantValidate.MSG_NOT_VALUE)
	private String virtualBranch;

	/**
	 * Código de virtual User.
	 */
	@ApiModelProperty(value = ConstantSwagger.VIRTUAL_USER, required = true)
	@NotNull(message = ConstantValidate.MSG_NOT_VALUE)
	private String virtualUser;

	/**
	 * Código de cargoTransaction.
	 */
	@ApiModelProperty(value = ConstantSwagger.CARGO_TRANSACCION, required = true)
	@NotNull(message = ConstantValidate.MSG_NOT_VALUE)
	private String cargoTransaction;

	/**
	 * Código de creditTransaction.
	 */
	@ApiModelProperty(value = ConstantSwagger.CREDIT_TRANSACCION, required = true)
	@NotNull(message = ConstantValidate.MSG_NOT_VALUE)
	private String creditTransaction;

	/**
	 * Código de business.
	 */
	@ApiModelProperty(value = ConstantSwagger.BUSINESS, required = true)
	@NotNull(message = ConstantValidate.MSG_NOT_VALUE)
	private String business;

	/**
	 * Código de branchTransaction.
	 */
	@ApiModelProperty(value = ConstantSwagger.BRANCH_TRANSACCTION, required = true)
	@NotNull(message = ConstantValidate.MSG_NOT_VALUE)
	private String branchTransaction;

	/**
	 * Código de Cheque.
	 */
	@ApiModelProperty(value = ConstantSwagger.CHECK, required = true)
	@NotNull(message = ConstantValidate.MSG_NOT_VALUE)
	private Integer check;

	/**
	 * Código de Moneda.
	 */
	@ApiModelProperty(value = ConstantSwagger.COIN, required = true)
	@NotNull(message = ConstantValidate.MSG_NOT_VALUE)
	private String coin;

	/**
	 * Código de usuario que autoriza.
	 */
	@ApiModelProperty(value = ConstantSwagger.USER_AUTHORIZES, required = true)
	@NotNull(message = ConstantValidate.MSG_NOT_VALUE)
	private String userAuthorizes;

	/**
	 * Cantidad SBC.
	 */
	@ApiModelProperty(value = ConstantSwagger.SBC_AMOUNT, required = true)
	@NotNull(message = ConstantValidate.MSG_NOT_VALUE)
	@JsonProperty(ApiValues.SBCAMOUNT)
	private BigDecimal sbcAmount;

	/**
	 * Cantidad restante.
	 */
	@ApiModelProperty(value = ConstantSwagger.REM_AMOUNT, required = true)
	@NotNull(message = ConstantValidate.MSG_NOT_VALUE)
	@JsonProperty(ApiValues.REMAMOUNT)
	private BigDecimal remAmount;

	/**
	 * Código retDays.
	 */
	@ApiModelProperty(value = ConstantSwagger.RET_DAYS, required = true)
	@NotNull(message = ConstantValidate.MSG_NOT_VALUE)
	@JsonProperty(ApiValues.RETDAYS)
	private Integer retDays;

	/**
	 * Documento.
	 */
	@ApiModelProperty(value = ConstantSwagger.DOCTO, required = true)
	@NotNull(message = ConstantValidate.MSG_NOT_VALUE)
	private Integer docto;

	/**
	 * Código de typeReversion.
	 */
	@ApiModelProperty(value = ConstantSwagger.TYPE_REVERSION, required = true)
	@NotNull(message = ConstantValidate.MSG_NOT_VALUE)
	private String typeReversion;

	/**
	 * Id del servicio a pagar.
	 */
	@ApiModelProperty(value = ConstantSwagger.TYPE_REVERSION, required = true)
	@NotNull(message = ConstantValidate.MSG_NOT_VALUE)
	private String idServicePayment;

	/**
	 * otp para valdarlo con gemalto token.
	 */
	@NotNull
	@NotEmpty
	@NotBlank
	private String otp;

}
