package com.bancoppel.pymt.reverse.transaction.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * POJO de response de api.
 *
 * @author nova
 *
 */
@Data
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class ApplyThirdPartyTransferResponse {

	/**
	 * Folio de rastreo.
	 */
	private String invoiceBranch;

}
