package com.bancoppel.pymt.reverse.transaction.config;

import java.util.concurrent.Executor;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

/**
 * Configuracion para crear un executor.
 *
 * @author DMR
 */
@Configuration
@EnableAsync
public class AsyncConfiguration {

  /**
   * Tamaño del pool de threads.
   */
  @Value("${executor.threads.core-pool-size}")
  private int corePoolSize;

  /**
   * Maximo tamaño del pool de threads.
   */
  @Value("${executor.threads.max-pool-size}")
  private int maxPoolSize;

  /**
   * Numero de hilos activos.
   */
  @Value("${executor.threads.keep-alive}")
  private int keepAlive;

  /**
   * Cantidad de hilos pendientes de ejecucion.
   */
  @Value("${executor.threads.queue-capacity}")
  private int queueCapacity;

  /**
   * Method to task executor.
   *
   * @return Executor.
   */
  @Bean(name = "taskExecutor")
  public Executor taskExecutor() {

    final ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();

    executor.setCorePoolSize(corePoolSize);
    executor.setMaxPoolSize(maxPoolSize);
    executor.setKeepAliveSeconds(keepAlive);
    executor.setQueueCapacity(queueCapacity);
    executor.setThreadNamePrefix("Close Session -");
    executor.initialize();

    return executor;

  }

}
