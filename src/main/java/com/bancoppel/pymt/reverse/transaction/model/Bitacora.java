package com.bancoppel.pymt.reverse.transaction.model;

import com.bancoppel.ptsv.log.domain.TransactionalLog;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class Bitacora {
	
	/**
	   * Operation Id.
	   */
	  private String operationId;

	  /**
	   * Tipo de log.
	   */
	  private String logType;

	  /**
	   * Número de cliente.
	   */
	  private String customerNumber;

	  /**
	   * HttpStatus.
	   */
	  private int httpStatus;

	  /**
	   * Code error.
	   */
	  private String code;

	  /**
	   * DeviceId.
	   */
	  private String deviceId;

	  /**
	   * Uuid.
	   */
	  private String uuid;

	  /**
	   * Datos genericos.
	   */
	  private String genericData;

	  /**
	   * Transaction log.
	   */
	  private TransactionalLog transactionLog;

}
