package com.bancoppel.pymt.reverse.transaction.exception;

import com.bancoppel.pymt.reverse.transaction.constant.ConstantValidate;

import lombok.Getter;

@Getter
public class InvalidGemaltoTokenException extends RuntimeException {

	/**
	 * UID por default de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor por default.
	 */
	public InvalidGemaltoTokenException() {
		super(ConstantValidate.EMPTY_STRING);
	}

	/**
	 * Constructor que recibe el mensaje de error.
	 * 
	 * @param message Mensaje con información acerca de la excepción.
	 */
	public InvalidGemaltoTokenException(String message) {
		super(message);
	}
}
