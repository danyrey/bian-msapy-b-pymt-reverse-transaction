package com.bancoppel.pymt.reverse.transaction.util;

import java.util.Enumeration;
import java.util.Objects;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bancoppel.pymt.reverse.transaction.constant.ApiConstant;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Util {
	
	/** Static variable of record. */
	  private static final Logger LOGGER = LoggerFactory.getLogger(Util.class);
	  
	  /**
	   * Private Constructor will prevent the instantiation of this class directly.
	   */
	  private Util() {}

	  /**
	   * Método para obtener un json String.
	   * 
	   * @param object.
	   * @return Json Serializado.
	   */
	  public static String getJson(Object object) {
	    String jsonString = null;
	    try {
	      ObjectMapper mapper = new ObjectMapper();
	      jsonString = mapper.writeValueAsString(object);
	    } catch (JsonProcessingException ex) {
	      LOGGER.error(ex.getMessage(), ex);
	    }

	    return jsonString;
	  }
	  
	  /**
	   * Método para imprimir los headers de la petición Http.
	   * 
	   * @param request.
	   */
	  public static void printHeaders(HttpServletRequest request) {
	    Enumeration<String> headerNames = request.getHeaderNames();

	    if (!Objects.isNull(headerNames)) {
	      while (headerNames.hasMoreElements()) {
	        String key = headerNames.nextElement();
	        String value = request.getHeader(key);
	        LOGGER.debug(ApiConstant.MSG_TO_LOG_HEADER, key, value);
	      }
	    }
	  }

}
