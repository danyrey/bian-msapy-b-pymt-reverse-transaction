package com.bancoppel.pymt.reverse.transaction.model;



import java.math.BigDecimal;

import javax.validation.constraints.NotNull;

import com.bancoppel.pymt.reverse.transaction.constant.ConstantValidate;

import lombok.Data;

/**
 * 
 * @author DMR.
 *
 *         Entidad de request para aplicar transferencia.
 */
@Data
public class ApplyThirdPartyTransferRequest {

	
	/**
	   * Número de Cuenta Origen.
	   */
	  @NotNull(message = ConstantValidate.MSG_NOT_VALUE)
	  private String originAccountNumber;

	  /**
	   * Número de Cliente Origen.
	   */
	  @NotNull(message = ConstantValidate.MSG_NOT_VALUE)
	  private String originCustomerNumber;

	  /**
	   * Número de Cuenta Destino.
	   */
	  @NotNull(message = ConstantValidate.MSG_NOT_VALUE)
	  private String destinationAccountNumber;

	  /**
	   * Cantidad.
	   */
	  @NotNull(message = ConstantValidate.MSG_NOT_VALUE)
	  private BigDecimal amount;

	  /**
	   * Concepto.
	   */
	  @NotNull(message = ConstantValidate.MSG_NOT_VALUE)
	  private String concept;
	  
	  /**
	   * Cantidad Total.
	   */
	  @NotNull(message = ConstantValidate.MSG_NOT_VALUE)
	  private BigDecimal totalAmount;
	  
	  /**
	   * Cantidad en Firme.
	   */
	  @NotNull(message = ConstantValidate.MSG_NOT_VALUE)
	  private BigDecimal firmAmount;
	  
	  /**
	   * Código de virtualBranch.
	   */
	  @NotNull(message = ConstantValidate.MSG_NOT_VALUE)
	  private String virtualBranch;
	  
	  /**
	   * Código de virtual User.
	  */ 
	  @NotNull(message = ConstantValidate.MSG_NOT_VALUE)
	  private String virtualUser;
	  
	  /**
	   * Código de cargoTransaction.
	   */
	  @NotNull(message = ConstantValidate.MSG_NOT_VALUE)
	  private String cargoTransaction;
	  
	  /**
	   * Código de creditTransaction.
	   */
	  @NotNull(message = ConstantValidate.MSG_NOT_VALUE)
	  private String creditTransaction;
	  
	  /**
	   * Código de business.
	   */
	  @NotNull(message = ConstantValidate.MSG_NOT_VALUE)
	  private String business;

	  /**
	   * Código de branchTransaction.
	   */
	  @NotNull(message = ConstantValidate.MSG_NOT_VALUE)
	  private String branchTransaction;
	  
	  /**
	   * Código de Cheque.
	   */
	  @NotNull(message = ConstantValidate.MSG_NOT_VALUE)
	  private Integer check;
	  
	  /**
	   * Código de Moneda.
	   */
	  @NotNull(message = ConstantValidate.MSG_NOT_VALUE)
	  private String coin;
	  
	  /**
	   * Código de usuario que autoriza.
	   */
	  @NotNull(message = ConstantValidate.MSG_NOT_VALUE)
	  private String userAuthorizes;
	  
	  /**
	   * Cantidad SBC.
	   */
	  @NotNull(message = ConstantValidate.MSG_NOT_VALUE)
	  private BigDecimal sbcAmount;
	  
	  /**
	   * Cantidad restante.
	   */
	  @NotNull(message = ConstantValidate.MSG_NOT_VALUE)
	  private BigDecimal remAmount;
	  
	  /**
	   * Código retDays.
	   */
	  @NotNull(message = ConstantValidate.MSG_NOT_VALUE)
	  private Integer retDays;
	  
	  /**
	   * Documento.
	   */
	  @NotNull(message = ConstantValidate.MSG_NOT_VALUE)
	  private Integer docto;
	  
	  /**
	   * Código de typeReversion.
	   */
	  @NotNull(message = ConstantValidate.MSG_NOT_VALUE)
	  private String typeReversion;
}
