package com.bancoppel.pymt.reverse.transaction.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.bancoppel.pymt.reverse.transaction.MainBianMsapyBPymtReverseTransaction;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ConfigClientAppTests {

  @Test
  public void applicationContextTest() {
      MainBianMsapyBPymtReverseTransaction.main(new String[] {});
  }
  
}
